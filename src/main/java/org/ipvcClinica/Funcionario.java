package org.ipvcClinica;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table
public class Funcionario implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="funcionario_seq")
    @SequenceGenerator(name="funcionario_seq", sequenceName="funcionario_seq", allocationSize = 1)
    @Column(name = "idfunc", unique = true)
    private int idfunc;
    @Column(name = "nome", nullable = false)
    private String nome;
    @Column(name = "datanascimento", nullable = false)
    private LocalDate datanascimento;
    @Column(name = "contacto", nullable = false)
    private String contacto;

    @Column(name = "morada", nullable = false)
    private String morada;

    @Column(name = "nif", nullable = false)
    private String nif;
    @Column(name = "numcc", nullable = false)
    private String numcc;


    @Column(name = "admin", nullable = false)
    private boolean admin;

    /**Relação um para um com o idConta-FK*/
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idconta", referencedColumnName = "idconta")
    private Conta idconta;


    public int getIdfunc() {
        return idfunc;
    }

    public void setIdfunc(int idfunc) {
        this.idfunc = idfunc;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getDatanascimento() {
        return datanascimento;
    }

    public void setDatanascimento(LocalDate datanascimento) {
        this.datanascimento = datanascimento;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }


    public String getNif() {
        return this.nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getNumcc() {
        return numcc;
    }

    public void setNumcc(String numcc) {
        this.numcc = numcc;
    }

    public boolean getAdmin() {
        return this.admin = admin;
    }
    public void setAdmin(boolean admin) {
        this.admin = admin;
    }


    public Conta getIdconta() {
        return idconta;
    }

    public void setIdconta(Conta idconta) {
        this.idconta = idconta;
    }

    public String getMorada() {
        return morada;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }


    /*------------------------------------------Métodos BD-------------------------------------------------------------*/

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");

    /**
     * Obtém o dentista pela ID da conta.
     *
     * */
    public static Funcionario getFuncionarioPorConta(int idconta) {

        EntityManager em = emf.createEntityManager();

        String query = "SELECT f FROM Funcionario f WHERE f.idconta.idconta = :idconta";

        TypedQuery<Funcionario> tq = em.createQuery(query, Funcionario.class);
        tq.setParameter("idconta", idconta);
        Funcionario f = null;

        try{
            f = tq.getSingleResult();
            return f;

        }catch(NoResultException ex){
            ex.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }

    /**
     * Insere um novo funcionário na base de dados.
     * @param nome
     * @param datanascimento
     * @param contacto
     * @param morada
     * @param nif
     * @param numCC
     * @param admin
     * @param idconta
     * */
    public static void addFuncionario(String nome, LocalDate datanascimento, String contacto, String morada, String nif, String numCC, boolean admin, int idconta){
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;

        /*Procura na BD a conta com a id obtida no input*/
        String query = "SELECT c FROM Conta c WHERE c.idconta = :idconta";

        TypedQuery<Conta> tq = em.createQuery(query, Conta.class);
        tq.setParameter("idconta", idconta);
        Conta conta = null;


        try{
            et = em.getTransaction();
            et.begin();

            /*Obtém a conta com esta id*/
            conta = tq.getSingleResult();

            Funcionario func = new Funcionario();

            func.setNome(nome);
            func.setDatanascimento(datanascimento);
            func.setContacto(contacto);
            func.setNif(nif);
            func.setAdmin(admin);
            func.setNumcc(numCC);
            func.setMorada(morada);
            func.setIdconta(conta);



            em.persist(func);
            et.commit();
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }
    }


    /**
     * Obtém a lista dos funcionários.
     *
     * */
    public static List<Funcionario> getListaFunc(){

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        List<Funcionario> listaFuncionarios = new ArrayList<>();


        Query q1 = em.createQuery("SELECT f FROM Funcionario f WHERE f.idfunc IS NOT NULL");
        List<Object> result = q1.getResultList();

        for(Object f : result){
            listaFuncionarios.add((Funcionario) f);
        }

        return listaFuncionarios;
    }


    /**
     * Obtém o funcionário pela ID da conta.
     *
     * */
    public static Funcionario getFuncionario(int id) {

        EntityManager em = emf.createEntityManager();

        String query = "SELECT f FROM Funcionario f WHERE f.idfunc=:idfunc";

        TypedQuery<Funcionario> tq = em.createQuery(query, Funcionario.class);
        tq.setParameter("idfunc", id);
        Funcionario f = null;

        try{
            f = tq.getSingleResult();
            return f;

        }catch(NoResultException ex){
            ex.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }

    /**
     * Altera as permissões do funcionário.
     * @param id
     * @param admin
     * */

    public static void alteraPermissoes(int id, boolean admin) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;
        Funcionario f = null;

        try{
            et = em.getTransaction();
            et.begin();
            f = em.find(Funcionario.class, id);

            f.setAdmin(admin);

            em.persist(f);
            et.commit();
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }
    }

    /**
     * Remove um funcionario da BD.
     *
     * */
    public static boolean deleteFunc(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        boolean removido = false;
        try{
            Funcionario f = em.find(Funcionario.class, id);

            em.getTransaction().begin();
            em.remove(f);
            em.getTransaction().commit();
            removido = true;


        }catch(Exception ex){
            if(em != null){
                System.out.println("ERRO");

            }
            ex.printStackTrace();
        }finally{
            em.close();
            return removido;
        }

    }


}
