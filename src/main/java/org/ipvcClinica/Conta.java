package org.ipvcClinica;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Conta implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="conta_seq")
    @SequenceGenerator(name="conta_seq", sequenceName="conta_seq", allocationSize = 1)
    @Column(name = "idconta", unique = true)
    private  int idconta;
    @Column(name = "email", nullable = false)
    private String email;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "tipo", nullable = false)
    private String tipo;

    @OneToOne(mappedBy = "idconta")
    private Funcionario funcionario;

    @OneToOne(mappedBy = "idconta")
    private Utente utente;

    @OneToOne(mappedBy = "idconta")
    private Dentista dentista;

    public int getIdconta() {
        return idconta;
    }

    public void setIdconta(int idconta) {
        this.idconta = idconta;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Utente getUtente() {
        return utente;
    }

    public void setUtente(Utente utente) {
        this.utente = utente;
    }

    public Dentista getDentista() {
        return dentista;
    }

    public void setDentista(Dentista dentista) {
        this.dentista = dentista;
    }

    public String getTipo() { return tipo; }

    public void setTipo(String tipo) { this.tipo = tipo; }

    /**---------------------------Métodos BD--------------*/

    public static Conta getContaPorCredenciais(String email) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();
        String query = "SELECT c FROM Conta c WHERE c.email LIKE CONCAT('%', :email, '%')";

        TypedQuery<Conta> tq = em.createQuery(query, Conta.class);
        tq.setParameter("email", email);
        Conta con = null;

        try{  //return strings?
            con = tq.getSingleResult();
            return con;
        }catch(NoResultException ex){
            ex.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }


    /**
     * Verifica se uma conta com determinado email já existe no sistema.
     * */
    public static boolean verificaEmail(String email) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        boolean existe;

        String selectQuery = "FROM Conta c WHERE c.email = :email";
        Query query = em.createQuery(selectQuery);
        query.setParameter("email", email);


        List<Conta> results = query.getResultList();

        if(results.isEmpty()){
            existe=false;
        } else {
            existe=true;
        }
        return existe;

    }

      /* Insere uma nova conta na base de dados.
      * @param email
     * @param password
     * */

    public static void addConta(String email, String password,String tipo){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;


        try{
            et = em.getTransaction();
            et.begin();

            Conta conta = new Conta();


            conta.setEmail(email);
            conta.setPassword(password);
            conta.setTipo(tipo);

            em.persist(conta);
            et.commit();

        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
        }finally{
            em.close();

        }
    }


    public static Conta getConta(int idconta) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();
        String query = "SELECT c FROM Conta c WHERE c.idconta = :idconta";

        TypedQuery<Conta> tq = em.createQuery(query, Conta.class);
        tq.setParameter("idconta", idconta);
        Conta conta = null;

        try{
            conta = tq.getSingleResult();

        }catch(NoResultException ex){
            ex.printStackTrace();
        }finally {
            em.close();

            return conta;
        }
    }

    /**
     * Remove uma conta da BD.
     *
     * */

    public static boolean deleteConta(int idconta) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        boolean removido = false;
        try{
            Conta c = em.find(Conta.class, idconta);

            if (c != null) {
                em.getTransaction().begin();
                em.remove(c);
                em.getTransaction().commit();

                removido = true;
            }


        }catch(Exception ex){
            if(em != null){
                System.out.println("ERRO");

            }
            ex.printStackTrace();
        }finally{
            em.close();
            return removido;
        }

    }





}
