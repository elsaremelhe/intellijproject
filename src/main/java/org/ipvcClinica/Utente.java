package org.ipvcClinica;

import org.hibernate.annotations.SQLUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table
public class Utente implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="utente_seq")
    @SequenceGenerator(name="utente_seq", sequenceName="utente_seq", allocationSize = 1)
    @Column(name = "idutente", unique = true)
    private int idutente;
    @Column(name = "nome", nullable = false)
    private String nome;
    @Column(name = "datanascimento", nullable = false)
    private LocalDate datanascimento;
    @Column(name = "contacto", nullable = false)
    private String contacto;

    @Column(name = "morada", nullable = false)
    private String morada;

    @Column(name = "nif", nullable = false)
    private String nif;
    @Column(name = "numcc", nullable = false)
    private String numcc;
    @Column(name = "historico")
    private String historico;

    /**Relação um para um com o idConta-FK*/
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idconta", referencedColumnName = "idconta")
    private Conta idconta;


    public int getIdutente() {
        return idutente;
    }

    public void setIdutente(int idutente) {
        this.idutente = idutente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getDatanascimento() {
        return datanascimento;
    }

    public void setDatanascimento(LocalDate datanascimento) {
        this.datanascimento = datanascimento;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getNumcc() {
        return numcc;
    }

    public void setNumcc(String numcc) {
        this.numcc = numcc;
    }

    public String getHistorico() {
        return historico;
    }

    public void setHistorico(String historico) {
        this.historico = historico;
    }

    public Conta getConta() {
        return idconta;
    }

    public void setConta(Conta idconta) {
        this.idconta = idconta;
    }

    public String getMorada() {
        return morada;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }


    /**
     * Obtém a lista de todas os utentes.
     *
     * */
    public static List<Utente> getListaUtentes(){

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        List<Utente> listaUtentes = new ArrayList<>();


        Query q1 = em.createQuery("SELECT u FROM Utente u WHERE u.idutente IS NOT NULL");
        List<Object> result = q1.getResultList();

        for(Object u : result){
            listaUtentes.add((Utente) u);
        }

        return listaUtentes;
    }


    public static Utente getUtente (int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();
        String query = "SELECT u FROM Utente u WHERE u.idutente = :id";

        TypedQuery<Utente> tq = em.createQuery(query, Utente.class);
        tq.setParameter("id", id);
        Utente ut = null;

        try{
            ut = tq.getSingleResult();
            //o print nao tem a info toda
            return ut;
        }catch(NoResultException ex){
            ex.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }


    public static Utente alterUtente(int id, String hist){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;

        Utente ut = em.find(Utente.class, id);

        try{

            em.getEntityManagerFactory().getCache().evictAll();
            et = em.getTransaction();
            et.begin();

            ut.setHistorico(hist);

            em.persist(ut);

            et.commit();

            return ut;

        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }


    /**
     * Altera o estado e sala de uma consulta
     * @param id
     * @param morada
     * @param contacto
     * */

    public static void alteraDados(int id, String morada, String contacto) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;
        Utente u = null;

        try {
            em.getEntityManagerFactory().getCache().evictAll();
            et = em.getTransaction();
            et.begin();
            u = em.find(Utente.class, id);

            u.setMorada(morada);
            u.setContacto(contacto);

            em.persist(u);
            et.commit();
        } catch (Exception ex) {
            if (et != null) {
                et.rollback();
            }
            ex.printStackTrace();
        } finally {
            em.close();
        }
    }

    /**
     * Insere um novo utente na base de dados.
     * @param nome
     * @param datanascimento
     * @param contacto
     * @param morada
     * @param nif
     * @param numCC
     * @param historico
     * @param idconta
     * */
    public static void addUtente(String nome, LocalDate datanascimento, String contacto,String morada,String nif,String numCC, String historico, int idconta){

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;

        /*Procura na BD a conta com a id obtida no input*/
        String query = "SELECT c FROM Conta c WHERE c.idconta = :idconta";

        TypedQuery<Conta> tq = em.createQuery(query, Conta.class);
        tq.setParameter("idconta", idconta);
        Conta conta = null;


        try{
            et = em.getTransaction();
            et.begin();

            /*Obtém a conta com esta id*/
            conta = tq.getSingleResult();

            Utente ut = new Utente();
            ut.setNome(nome);
            ut.setDatanascimento(datanascimento);
            ut.setContacto(contacto);
            ut.setNif(nif);
            ut.setNumcc(numCC);
            ut.setMorada(morada);
            ut.setHistorico(historico);
            ut.setConta(conta);



            em.persist(ut);
            et.commit();
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }
    }

    /**
     * Obtém o utente pela ID da conta.
     *
     * */
    public static Utente getUtentePorConta(int idconta) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        String query = "SELECT u FROM Utente u WHERE u.idconta.idconta = :idconta";

        TypedQuery<Utente> tq = em.createQuery(query, Utente.class);
        tq.setParameter("idconta", idconta);
        Utente u = null;

        try{
            u = tq.getSingleResult();
            return u;

        }catch(NoResultException ex){
            ex.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }



}
