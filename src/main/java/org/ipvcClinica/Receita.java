package org.ipvcClinica;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Receita implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="receita_seq")
    @SequenceGenerator(name="receita_seq", sequenceName="receita_seq", allocationSize = 1)
    @Column(name = "idreceita", unique = true)
    private int idreceita;

    @Column(name = "medicacao", nullable = false)
    private String medicacao;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "quantidade", nullable = false)
    private int quantidade;

    @JoinColumn(name = "idconsulta", referencedColumnName = "idconsulta")
    @ManyToOne(optional = false)
    private Consulta idconsulta;

    public int getIdreceita() {
        return idreceita;
    }

    public void setIdreceita(int idreceita) {
        this.idreceita = idreceita;
    }

    public String getMedicacao() {
        return medicacao;
    }

    public void setMedicacao(String medicacao) {
        this.medicacao = medicacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }


    public Consulta getIdconsulta() {
        return idconsulta;
    }

    public void setIdconsulta(Consulta idconsulta) {
        this.idconsulta = idconsulta;
    }

    //-------------------MÉTODOS BD----------------------------------------------------------------

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");

    public static List<Receita> getListaReceitas(int idConsulta){
        EntityManager em = emf.createEntityManager();

        List<Receita> listaReceitas = new ArrayList<>();

        Query q1 = em.createQuery("SELECT r FROM Receita r WHERE r.idconsulta=" + idConsulta);
        List<Object> result = q1.getResultList();

        for(Object r : result){
            listaReceitas.add((Receita) r);
        }

        return listaReceitas;
    }

    public static Receita criarReceita(String medicacao, String descricao, String quantidade, int idconsulta){
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;

        /*Procura na BD a conta com a id obtida no input*/
        String query = "SELECT c FROM Consulta c WHERE c.idconsulta = :idconsulta";

        TypedQuery<Consulta> tq = em.createQuery(query, Consulta.class);
        tq.setParameter("idconsulta", idconsulta);
        Consulta con = null;


        try{
            et = em.getTransaction();
            et.begin();

            /*Obtém a consulta com esta id*/
            con = tq.getSingleResult();

            Receita r = new Receita();

            r.setMedicacao(medicacao);
            r.setDescricao(descricao);
            r.setQuantidade(Integer.valueOf(quantidade));
            r.setIdconsulta(con);

            em.persist(r);
            et.commit();

            return r;
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }

    public static void deleteReceita(int id) {
        EntityManager em = emf.createEntityManager();

        try{
            Receita rec = em.find(Receita.class, id);

            em.getTransaction().begin();
            em.remove(rec);
            em.getTransaction().commit();

        }catch(Exception ex){
            if(em != null){
                System.out.println("ERRO");
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }

    }
}
