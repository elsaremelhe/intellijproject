package org.ipvcClinica;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;


@Entity
@Table
public class Consulta implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="consulta_seq")
    @SequenceGenerator(name="consulta_seq", sequenceName="consulta_seq", allocationSize = 1)
    @Column(name = "idconsulta", unique = true)
    private int idconsulta;

    @Column(name = "estado", nullable = false)
    private String estado;

    @Column(name = "datainicio", nullable = false, columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private LocalDateTime datainicio;

    @Column(name = "datafim", nullable = true, columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private LocalDateTime datafim;

    @Column(name = "sala", nullable = false)
    private int sala;

    @JoinTable(name = "produtousado", joinColumns = {
            @JoinColumn(name = "idconsulta", referencedColumnName = "idconsulta")}, inverseJoinColumns = {
            @JoinColumn(name = "idprod", referencedColumnName = "idprod")})

    @ManyToMany
    private Collection<Produto> produtoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idconsulta")
    private Collection<Fatura> faturaCollection;

    @JoinColumn(name = "iddent", referencedColumnName = "iddent")
    @ManyToOne(optional = false)
    private Dentista iddent;

    @JoinColumn(name = "idutente", referencedColumnName = "idutente")
    @ManyToOne(optional = false)
    private Utente idutente;

    @JoinColumn(name = "idproced", referencedColumnName = "idproced")
    @ManyToOne(optional = false)
    private Procedimento procedimento;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idconsulta")
    private Collection<Receita> receitaCollection;

    @OneToMany(mappedBy = "idconsulta")
    Set<ProdutoUsado> produtosusados;


    public int getIdconsulta() {
        return idconsulta;
    }

    public void setIdconsulta(int idconsulta) {
        this.idconsulta = idconsulta;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public LocalDateTime getDatainicio() {
        return  datainicio;
    }

    public void setDatainicio(LocalDateTime datainicio) {
        this.datainicio = datainicio;
    }

    public LocalDateTime getDatafim() {
        return datafim;
    }

    public void setDatafim(LocalDateTime datafim) {
        this.datafim = datafim;
    }

    public int getSala() {
        return sala;
    }

    public void setSala(int sala) {
        this.sala = sala;
    }

    public Collection<Produto> getProdutoCollection() {
        return produtoCollection;
    }

    public void setProdutoCollection(Collection<Produto> produtoCollection) {
        this.produtoCollection = produtoCollection;
    }

    public Collection<Fatura> getFaturaCollection() {
        return faturaCollection;
    }

    public void setFaturaCollection(Collection<Fatura> faturaCollection) {
        this.faturaCollection = faturaCollection;
    }

    public Dentista getIddent() {
        return iddent;
    }

    public void setIddent(Dentista iddent) {
        this.iddent = iddent;
    }

    public Utente getIdutente() {
        return idutente;
    }

    public void setIdutente(Utente idutente) {
        this.idutente = idutente;
    }


    public Procedimento getProcedimento() {
        return procedimento;
    }

    public void setProcedimento(Procedimento procedimento) {
        this.procedimento = procedimento;
    }

    public Collection<Receita> getReceitaCollection() {
        return receitaCollection;
    }

    public void setReceitaCollection(Collection<Receita> receitaCollection) {
        this.receitaCollection = receitaCollection;
    }



    //--------------------------------------- FUNÇÕES BD ----------------------------------------

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");


    public static Consulta getConsulta(int id) {
        EntityManager em = emf.createEntityManager();
        String query = "SELECT c FROM Consulta c WHERE c.idconsulta = :id";

        TypedQuery<Consulta> tq = em.createQuery(query, Consulta.class);
        tq.setParameter("id", id);
        Consulta cons = null;

        try{
            cons = tq.getSingleResult();
            return cons;
        }catch(NoResultException ex){
            ex.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }


    /**
     * Obtém a lista de todas as consultas no sistema.
     * */
    public static List<Consulta> getListaConsultas(){


        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        List<Consulta> listaConsultas = new ArrayList<>();


        Query q1 = em.createQuery("SELECT c FROM Consulta c WHERE c.idconsulta IS NOT NULL");
        List<Object> result = q1.getResultList();

        for(Object c : result){
            listaConsultas.add((Consulta)c);
        }

        return listaConsultas;
    }

    /**
     * Obtém a lista de todas as consultas de determinado dentista.
     * @param idconta
     * */
    public static List<Consulta> getConsultasPorDentista(int idconta){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        List<Consulta> listaConsultas = new ArrayList<>();

        //por alguma razao usar :idconta ou ? e setParameter nao dá
        Query q1 = em.createQuery("SELECT c FROM Consulta c, Dentista d WHERE c.iddent = d.iddent AND d.idconta = " + idconta);

        //q1.setParameter("idconta", idconta);
        //q1.setParameter(1,idconta);
        List<Object> result = q1.getResultList();

        for(Object c : result){
            listaConsultas.add((Consulta)c);
            //System.out.println(c);
        }

        return listaConsultas;
    }

    /**
     *  Obtém a lista das consultas cujo estado é "Confirmada" de determinado dentista, ordenadas por data.
     * @param idconta
     * */
    public static List<Consulta> getConsultasPorDentistaConfirmadas(int idconta){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        List<Consulta> listaConsultas = new ArrayList<>();

        //por alguma razao usar :idconta ou ? e setParameter nao dá
        Query q1 = em.createQuery("SELECT c FROM Consulta c, Dentista d WHERE c.estado='Confirmada' AND c.iddent = d.iddent AND d.idconta = " + idconta + " ORDER BY c.datainicio");

        //q1.setParameter("idconta", idconta);
        //q1.setParameter(1,idconta);
        List<Object> result = q1.getResultList();

        for(Object c : result){
            listaConsultas.add((Consulta)c);
            //System.out.println(c);
        }

        return listaConsultas;
    }


    /**
     *  Obtém a lista das consultas cuja data é referente ao dia atual, de determinado dentista, ordenadas por hora.
     * @param idconta
     * */
    public static List<Consulta> getConsultasPorDentistaHoje(int idconta){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        List<Consulta> listaConsultas = new ArrayList<>();

        List<Consulta> listaConsultasHoje = new ArrayList<>();


        //por alguma razao usar :idconta ou ? e setParameter nao dá
        Query q1 = em.createQuery("SELECT c FROM Consulta c, Dentista d WHERE c.iddent = d.iddent AND d.idconta = " + idconta + " ORDER BY c.datainicio");

        //q1.setParameter("idconta", idconta);
        //q1.setParameter(1,idconta);
        List<Object> result = q1.getResultList();

        for(Object c : result){
            listaConsultas.add((Consulta)c);
            //System.out.println(c);
        }


        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        String data;
        String today;

        for (Consulta c: listaConsultas){

            data = formatterData.format(c.getDatainicio());
            today = formatterData.format(LocalDateTime.now());

            //System.out.println(data);
            //System.out.println(today);

            if (data.equals(today)){
                listaConsultasHoje.add(c);
            }
        }

        return listaConsultasHoje;
    }

    /**
     *  Obtém a lista das consultas cuja data é anterior ao dia atual, ou seja, o histórico de consultas de determinado dentista,
     *  ordenadas por data(da mais recente à mais antiga).
     * @param idconta
     * */
    public static List<Consulta> getConsultasPorDentistaHistorico(int idconta){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        List<Consulta> listaConsultas = new ArrayList<>();

        List<Consulta> listaHistorico = new ArrayList<>();


        //por alguma razao usar :idconta ou ? e setParameter nao dá
        Query q1 = em.createQuery("SELECT c FROM Consulta c, Dentista d WHERE c.iddent = d.iddent AND d.idconta = " + idconta + " ORDER BY c.datainicio DESC");

        //q1.setParameter("idconta", idconta);
        //q1.setParameter(1,idconta);
        List<Object> result = q1.getResultList();

        for(Object c : result){
            listaConsultas.add((Consulta)c);
            //System.out.println(c);
        }


        LocalDateTime data;
        LocalDateTime today;

        for (Consulta c: listaConsultas){

            data = c.getDatainicio();
            today = LocalDateTime.now();

            if (data.isBefore(today)){
                listaHistorico.add(c);
            }
        }

        return listaHistorico;
    }


    /**
     *  Obtém a lista de todas as consultas do sistema cujo estado é "Confirmada", ordenadas por data.
     * */
    public static List<Consulta> getConsultasConfirmadas(){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        List<Consulta> listaConsultas = new ArrayList<>();


        Query q1 = em.createQuery("SELECT c FROM Consulta c WHERE c.estado='Confirmada' ORDER BY c.datainicio");


        List<Object> result = q1.getResultList();

        for(Object c : result){
            listaConsultas.add((Consulta)c);

        }

        return listaConsultas;
    }

    /**
     *  Obtém a lista de todas as consultas do sistema cuja data é referente ao dia atual ordenadas por hora.
     * */
    public static List<Consulta> getConsultasHoje(){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        List<Consulta> listaConsultas = new ArrayList<>();

        List<Consulta> listaConsultasHoje = new ArrayList<>();



        Query q1 = em.createQuery("SELECT c FROM Consulta c ORDER BY c.datainicio");


        List<Object> result = q1.getResultList();

        for(Object c : result){
            listaConsultas.add((Consulta)c);

        }

        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        String data;
        String today;

        for (Consulta c: listaConsultas){

            data = formatterData.format(c.getDatainicio());
            today = formatterData.format(LocalDateTime.now());

            //System.out.println(data);
            //System.out.println(today);

            if (data.equals(today)){
                listaConsultasHoje.add(c);
            }
        }

        return listaConsultasHoje;
    }

    /**
     *  Obtém a lista das consultas cuja data é anterior ao dia atual, ou seja, o histórico de consultas de determinado dentista,
     *  ordenadas por data(da mais recente à mais antiga).
     * */
    public static List<Consulta> getConsultasHistorico(){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        List<Consulta> listaConsultas = new ArrayList<>();

        List<Consulta> listaHistorico = new ArrayList<>();



        Query q1 = em.createQuery("SELECT c FROM Consulta c ORDER BY c.datainicio DESC");


        List<Object> result = q1.getResultList();

        for(Object c : result){
            listaConsultas.add((Consulta)c);

        }


        LocalDateTime data;
        LocalDateTime today;

        for (Consulta c: listaConsultas){

            data = c.getDatainicio();
            today = LocalDateTime.now();

            if (data.isBefore(today)){
                listaHistorico.add(c);
            }
        }

        return listaHistorico;
    }

    /**
     * Altera o estado e sala de uma consulta
     * @param id
     * @param estado
     * */
    public static void alteraEstado(int id, int sala, String estado) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;
        Consulta con = null;

        try{
            et = em.getTransaction();
            et.begin();
            con = em.find(Consulta.class, id);

            con.setSala(sala);
            con.setEstado(estado);

            em.persist(con);
            et.commit();
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }
    }

    public static void addConsulta(String dataInicio,String horaInicio, int sala, int iddent, int idutente, int idproced){
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;

        /*Procura na BD a conta com a id obtida no input*/
        String queryD = "SELECT d FROM Dentista d WHERE d.iddent = :iddent";
        String queryU = "SELECT u FROM Utente u WHERE u.idutente = :idutente";
        String queryP = "SELECT p FROM Procedimento p WHERE p.idproced = :idproced";

        TypedQuery<Dentista> tq = em.createQuery(queryD, Dentista.class);
        tq.setParameter("iddent", iddent);

        TypedQuery<Utente> tqU = em.createQuery(queryU, Utente.class);
        tqU.setParameter("idutente", idutente);

        TypedQuery<Procedimento> tqP = em.createQuery(queryP, Procedimento.class);
        tqP.setParameter("idproced", idproced);


        Consulta con = null;

        Dentista d = null;
        Utente u = null;
        Procedimento p = null;


        try{
            et = em.getTransaction();
            et.begin();

            /*Obtém a consulta com esta id*/
            d = tq.getSingleResult();
            u = tqU.getSingleResult();
            p = tqP.getSingleResult();

            String str = dataInicio + " " + horaInicio;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
            LocalDateTime dateTime = LocalDateTime.parse(str, formatter);

            Consulta c = new Consulta();
            c.setEstado("Confirmada");
            c.setDatainicio(dateTime);
            c.setDatafim(null);
            c.setSala(sala);
            c.setIddent(d);
            c.setIdutente(u);
            c.setProcedimento(p);


            em.persist(c);
            et.commit();
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }
    }
    /**
     * Obtém a lista de todas as consultas de determinado utente(histórico).
     * @param idutente
     * */

    public static List<Consulta> getConsultasPorUtente(int idutente){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        List<Consulta> listaConsultas = new ArrayList<>();
        List<Consulta> listaHistorico = new ArrayList<>();


        Query q1 = em.createQuery("SELECT c FROM Consulta c, Utente u WHERE c.idutente = u.idutente AND c.idutente=" + idutente + " ORDER BY c.datainicio DESC" );


        List<Object> result = q1.getResultList();

        for(Object c : result){
            listaConsultas.add((Consulta)c);
            //System.out.println(c);
        }


        LocalDateTime data;
        LocalDateTime today;

        for (Consulta c: listaConsultas){

            data = c.getDatainicio();
            today = LocalDateTime.now();

            if (data.isBefore(today)){
                listaHistorico.add(c);
            }
        }

        return listaHistorico;

    }


}
