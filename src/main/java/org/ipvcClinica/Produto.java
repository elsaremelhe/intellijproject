package org.ipvcClinica;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Entity
@Table
public class Produto implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="produto_seq")
    @SequenceGenerator(name="produto_seq", sequenceName="produto_seq", allocationSize = 1)
    @Column(name = "idprod", unique = true)
    private int idprod;
    @Column(name = "nome", nullable = false)
    private String nome;
    @Column(name = "quantidade", nullable = false)
    private int quantidade;

    @OneToMany(mappedBy = "idprod")
    Set<ProdutoUsado> produtosusados;

    public int getIdprod() {
        return idprod;
    }

    public void setIdprod(int idprod) {
        this.idprod = idprod;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }



    //-------------------MÉTODOS BD----------------------------------------------------------------

    /*
    * Obtém a lista de produtos do sistema.
    * */
    public static List<Produto> getListaProdutos(){


        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        List<Produto> listaProdutos = new ArrayList<>();


        Query q1 = em.createQuery("SELECT p FROM Produto p WHERE p.idprod IS NOT NULL");
        List<Object> result = q1.getResultList();

        for(Object p : result){
            listaProdutos.add((Produto)p);
        }

        return listaProdutos;

    }

    /**
     * Altera a quantidade em stock de um produto.
     * @param id
     * @param quantidade
     * */
    public static void alteraStock(int id, int quantidade) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;
        Produto p = null;

        try{
            et = em.getTransaction();
            et.begin();
            p = em.find(Produto.class, id);

            p.setQuantidade(quantidade);

            em.persist(p);
            et.commit();
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }
    }

}
