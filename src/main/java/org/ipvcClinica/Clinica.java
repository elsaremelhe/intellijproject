package org.ipvcClinica;

import javax.persistence.*;
import java.io.Serializable;



@Entity
@Table
public class Clinica  implements  Serializable{

        private static final long serialVersionUID = 1L;

        @Id
        @Basic(optional = false)
        @Column(name = "idclinica", unique = true)
        private  int idclinica;
        @Column(name = "nome", nullable = false)
        private String nome;
        @Column(name = "morada", nullable = false)
        private String morada;
        @Column(name = "nif", nullable = false)
        private String nif;

    public int getIdclinica() {
        return idclinica;
    }

    public void setIdclinica(int idclinica) {
        this.idclinica = idclinica;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMorada() {
        return morada;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }


/**--------------------------Métodos BD---------------------------------------------------------------------------------/
 *
 */

    /**
     * Obtém uma clínica pela sua ID.
     * */

    public static Clinica getClinica(int id) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();
        String query = "SELECT c FROM Clinica c WHERE c.idclinica = :idclinica";

        TypedQuery<Clinica> tq = em.createQuery(query, Clinica.class);
        tq.setParameter("idclinica", id);
        Clinica clinica = null;

        try{
            clinica = tq.getSingleResult();

        }catch(NoResultException ex){
            ex.printStackTrace();
        }finally {
            em.close();

            return clinica;
        }
    }
}

