package org.ipvcClinica;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table
//@IdClass(ProdutoUsadoPK.class)
public class ProdutoUsado implements Serializable {

    private static final long serialVersionUID = 1L;


    @EmbeddedId
    protected ProdutoUsadoPK produtousadoPK;


    @ManyToOne
    @MapsId("idconsulta")
    @JoinColumn(name = "idconsulta")
    //@JoinColumn(name = "idconsulta",insertable = false, updatable = false)
            Consulta idconsulta;


    @ManyToOne
    @MapsId("idprod")
    @JoinColumn(name = "idprod")
    //@JoinColumn(name = "idprod",insertable = false, updatable = false)
            Produto idprod;

    @Basic(optional = false)
    @Column(name = "quantidade")
    private int quantidade;

    public ProdutoUsado() {
    }

    public ProdutoUsado(ProdutoUsadoPK produtousadoPK) {
        this.produtousadoPK = produtousadoPK;
    }

    public ProdutoUsado(ProdutoUsadoPK produtousadoPK, int quantidade) {
        this.produtousadoPK = produtousadoPK;
        this.quantidade = quantidade;
    }

    public ProdutoUsado(int idprod, int idconsulta) {
        this.produtousadoPK = new ProdutoUsadoPK(idprod, idconsulta);
    }

    public ProdutoUsadoPK getProdutousadoPK() {
        return produtousadoPK;
    }

    public void setProdutoUsadoPK(ProdutoUsadoPK produtousadoPK) {
        this.produtousadoPK = produtousadoPK;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Consulta getConsulta() {
        return idconsulta;
    }

    public void setConsulta(Consulta consulta) {
        this.idconsulta = consulta;
    }

    public Produto getProduto() {
        return idprod;
    }

    public void setProduto(Produto produto) {
        this.idprod = produto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass())
            return false;

        ProdutoUsado that = (ProdutoUsado) o;
        return Objects.equals(idconsulta, that.idconsulta) &&
                Objects.equals(idprod, that.idprod);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idconsulta, idprod);
    }

    //-------------------MÉTODOS BD----------------------------------------------------------------

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");

    public static List<ProdutoUsado> getListaProdsUsados(int idConsulta){
        EntityManager em = emf.createEntityManager();

        List<ProdutoUsado> listaProdsUsados = new ArrayList<>();

        Query q1 = em.createQuery("SELECT pu FROM ProdutoUsado pu WHERE pu.idconsulta=" + idConsulta);
        List<Object> result = q1.getResultList();

        for(Object pu : result){
            listaProdsUsados.add((ProdutoUsado) pu);
        }

        return listaProdsUsados;
    }


    public static ProdutoUsado addProdUsado(Consulta idconsulta, Produto idproduto, String quantidade){
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;

        try{
            et = em.getTransaction();
            et.begin();

            ProdutoUsado pu = new ProdutoUsado();
            pu.setConsulta(idconsulta);
            pu.setProduto(idproduto);
            pu.setQuantidade(Integer.valueOf(quantidade));
            pu.setProdutoUsadoPK(new ProdutoUsadoPK(idproduto.getIdprod(), idconsulta.getIdconsulta()));

            em.merge(pu);
            et.commit();

            return pu;
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }

    public static void deleteProdUsado(ProdutoUsadoPK id) {
        EntityManager em = emf.createEntityManager();

        try{
            ProdutoUsado pu = em.find(ProdutoUsado.class, id);

            em.getTransaction().begin();
            em.remove(pu);
            em.getTransaction().commit();

        }catch(Exception ex){
            if(em != null){
                System.out.println("ERRO");
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }

    }

    public static ProdutoUsado updateProdutoUsado(int idconsulta, int idproduto, String qtd){
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;
        String query = "SELECT pu FROM ProdutoUsado pu WHERE pu.idconsulta = " + idconsulta + " AND pu.idprod = " + idproduto;

         TypedQuery<ProdutoUsado> tq = em.createQuery(query, ProdutoUsado.class);
         ProdutoUsado pu = null;

        try{
            //ProdutoUsado pu = em.find(ProdutoUsado.class, id);

            pu = tq.getSingleResult();
            em.getEntityManagerFactory().getCache().evictAll();
            et = em.getTransaction();
            et.begin();

            pu.setQuantidade(pu.getQuantidade() + Integer.valueOf(qtd));

            em.persist(pu);

            et.commit();

            return pu;

        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }
}
