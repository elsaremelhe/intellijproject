package org.ipvcClinica;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ProdutoUsadoPK implements Serializable {


    @Column(name = "idprod")
    private int idprod;

    @Column(name = "idconsulta")
    private int idconsulta;

    public ProdutoUsadoPK() {

    }

    public ProdutoUsadoPK(int idprod, int idconsulta) {
        this.idprod = idprod;
        this.idconsulta = idconsulta;
    }

    public int getIdprod() {
        return idprod;
    }

    public void setIdprod(int idprod) {
        this.idprod = idprod;
    }

    public int getIdconsulta() {
        return idconsulta;
    }

    public void setIdconsulta(int idconsulta) {
        this.idconsulta = idconsulta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProdutoUsadoPK)) return false;
        ProdutoUsadoPK that = (ProdutoUsadoPK) o;
        return getIdprod() == that.getIdprod() &&
                getIdconsulta() == that.getIdconsulta();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdprod(), getIdconsulta());
    }
}
