package org.ipvcClinica;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Procedimento implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="procedimento_seq")
    @SequenceGenerator(name="procedimento_seq", sequenceName="procedimento_seq", allocationSize = 1)
    @Column(name = "idproced", unique = true)
    private int idproced;
    @Column(name = "nome", nullable = false)
    private String nome;
    @Column(name = "preco", nullable = false)
    private double preco;
    @Column(name = "duracao")
    private int duracao;


    public int getIdproced() {
        return idproced;
    }

    public void setIdproced(int idproced) {
        this.idproced = idproced;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public int getDuracao() {
        return duracao;
    }

    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }

    //-------------------MÉTODOS BD----------------------------------------------------------------

    /*
     * Obtém a lista de procedimentos do sistema.
     * */
    public static List<Procedimento> getListaProcedimentos(){


        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        List<Procedimento> listaProcedimentos = new ArrayList<>();


        Query q1 = em.createQuery("SELECT p FROM Procedimento p WHERE p.idproced IS NOT NULL");
        List<Object> result = q1.getResultList();

        for(Object p : result){
            listaProcedimentos.add((Procedimento) p);
        }

        return listaProcedimentos;

    }

    /**
     * Remove um procedimento da BD.
     *
     * */

    public static boolean deleteProced(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        boolean removido = false;
        try{
            Procedimento p = em.find(Procedimento.class, id);

            em.getTransaction().begin();
            em.remove(p);
            em.getTransaction().commit();
            removido = true;


        }catch(Exception ex){
            if(em != null){
                System.out.println("ERRO");

            }
            ex.printStackTrace();
        }finally{
            em.close();
            return removido;
        }

    }
}
