package org.ipvcClinica;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table
public class Dentista implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="dentista_seq")
    @SequenceGenerator(name="dentista_seq", sequenceName="dentista_seq", allocationSize = 1)
    @Column(name = "iddent", unique = true)
    private int iddent;
    @Column(name = "nome", nullable = false)
    private String nome;
    @Column(name = "datanascimento", nullable = false)
    private LocalDate datanascimento;
    @Column(name = "contacto", nullable = false)
    private String contacto;

    @Column(name = "morada", nullable = false)
    private String morada;

    @Column(name = "nif", nullable = false)
    private String nif;
    @Column(name = "numcc", nullable = false)
    private String numcc;

    /**Relação um para um com o idConta-FK*/
    @OneToOne
    @JoinColumn(name = "idconta", referencedColumnName = "idconta")
    private Conta idconta;


    public int getIddent() {
        return iddent;
    }

    public void setIddent(int iddent) {
        this.iddent = iddent;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getDatanascimento() {
        return datanascimento;
    }

    public void setDatanascimento(LocalDate datanascimento) {
        this.datanascimento = datanascimento;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getNumcc() {
        return numcc;
    }

    public void setNumcc(String numcc) {
        this.numcc = numcc;
    }

    public Conta getConta() {
        return idconta;
    }

    public void setConta(Conta idconta) {
        this.idconta = idconta;
    }

    public String getMorada() {
        return morada;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    /*---------------------Métodos BD------------------------------------------------------------------------*/


    /**
     * Obtém a lista de todas os dentistas.
     *
     * */
    public static List<Dentista> getListaDentistas(){

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        List<Dentista> listaDentistas = new ArrayList<>();


        Query q1 = em.createQuery("SELECT d FROM Dentista d WHERE d.iddent IS NOT NULL AND d.idconta IS NOT NULL");
        List<Object> result = q1.getResultList();

        for(Object d : result){
            listaDentistas.add((Dentista) d);
        }

        return listaDentistas;
    }

    /**
     * Obtém o dentista pela ID da conta.
     *
     * */
    public static Dentista getDentistaPorConta(int idconta) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        String query = "SELECT d FROM Dentista d WHERE d.idconta.idconta = :idconta";

        TypedQuery<Dentista> tq = em.createQuery(query, Dentista.class);
        tq.setParameter("idconta", idconta);
        Dentista d = null;

        try{
            d = tq.getSingleResult();
            return d;

        }catch(NoResultException ex){
            ex.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }


    /**
     * Insere um novo dentista na base de dados.
     * @param nome
     * @param datanascimento
     * @param contacto
     * @param morada
     * @param nif
     * @param numCC
     * @param idconta
     * */
    public static void addDentista(String nome, LocalDate datanascimento, String contacto,String morada,String nif,String numCC, int idconta){

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;

        /*Procura na BD a conta com a id obtida no input*/
        String query = "SELECT c FROM Conta c WHERE c.idconta = :idconta";

        TypedQuery<Conta> tq = em.createQuery(query, Conta.class);
        tq.setParameter("idconta", idconta);
        Conta conta = null;


        try{
            et = em.getTransaction();
            et.begin();

            /*Obtém a conta com esta id*/
            conta = tq.getSingleResult();

            Dentista d = new Dentista();
            d.setNome(nome);
            d.setDatanascimento(datanascimento);
            d.setContacto(contacto);
            d.setNif(nif);
            d.setNumcc(numCC);
            d.setMorada(morada);

            d.setConta(conta);



            em.persist(d);
            et.commit();
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }
    }

    /**
     * Obtém o dentista pela ID da conta.
     *
     * */
    public static Dentista getDentista(int iddent) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");
        EntityManager em = emf.createEntityManager();

        String query = "SELECT d FROM Dentista d WHERE d.iddent=:iddent";

        TypedQuery<Dentista> tq = em.createQuery(query, Dentista.class);
        tq.setParameter("iddent", iddent);
        Dentista d = null;

        try{
            d = tq.getSingleResult();
            return d;

        }catch(NoResultException ex){
            ex.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }


}
