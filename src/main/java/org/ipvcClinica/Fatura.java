package org.ipvcClinica;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.sql.Time;
import java.util.Calendar;


@Entity
@Table
public class Fatura implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idfatura_seq")
    @SequenceGenerator(name="idfatura_seq", sequenceName="idfatura_seq", allocationSize = 1)
    @Column(name = "idfatura", unique = true)
    private int idfatura;
    @Column(name = "dataemissao", nullable = false)
    @Temporal(TemporalType.TIMESTAMP) //?
    private Date dataemissao;
    @Column(name = "descricao", nullable = false)
    private String descricao;
    @Column(name = "total", nullable = false)
    private double total;
    @JoinColumn(name = "idconsulta", referencedColumnName = "idconsulta")
    @ManyToOne(optional = false)
    private Consulta idconsulta;

    public int getIdfatura() {
        return idfatura;
    }

    public void setIdfatura(int idfatura) {
        this.idfatura = idfatura;
    }

    public Date getDataemissao() {
        return dataemissao;
    }

    public void setDataemissao(Date dataemissao) {
        this.dataemissao = dataemissao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Consulta getIdconsulta() {
        return idconsulta;
    }

    public void setIdconsulta(Consulta idconsulta) {
        this.idconsulta = idconsulta;
    }


    //-----------------------------------Métodos BD-----------------------------------------------------------------//

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");

    /**
     * Obtém a fatura de uma determinada consulta.
     * @param idconsulta
     * */
    public static Fatura getFatura(int idconsulta) {
        EntityManager em = emf.createEntityManager();
        String query = "SELECT f FROM Fatura f WHERE f.idconsulta.idconsulta = :idconsulta";

        TypedQuery<Fatura> tq = em.createQuery(query, Fatura.class);
        tq.setParameter("idconsulta", idconsulta);
        Fatura f = null;

        try{
            f = tq.getSingleResult();
            return f;
        }catch(NoResultException ex){
            ex.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }

}
