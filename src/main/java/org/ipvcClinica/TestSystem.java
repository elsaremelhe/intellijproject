package org.ipvcClinica;


import javax.persistence.*;
import java.sql.SQLOutput;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TestSystem {
    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto2Maven");

    public static void main(String[] args){
        //addProcedimento("Remoção de Siso", 33.5, 1);
        //getProcedimento(3);
        //changeProcedimento(999,"Ola",30,5);
        //addProd("Espelho Dental", 40);
        //getProcedimentos();
        //deleteProcedimento(1);
        //addConta(1,"maria@gmail.pt","12345");
        //addConta(2,"joao@hotmail.com","444");
        //addConta("joana@sapo.pt","222");
        //addConta("teste@sapo.pt","551");
        //getProdUsado(1);

        //deleteConta(1);

        //addFuncionario("Maria Almeida",LocalDate.of(1975,3,24),"91722833923","Arcos de Valdevez","430495849589","2303949348 ZYS",true,4);

        //getContaPorEmail("anaramos@nomail.com");
        //getConta(2);
        //getFuncionario(1);
        //getListaFuncionarios();

        //addUtente("Rui Paiva", LocalDate.of(1990,6,1),"919823777","Viana do Castelo","234454233355","82733461 XYY","Alergia a pinincilina",4);
        //getUtente(1);
        //getListaUtentes();
        //getProduto(1);
        //getListaProdutos();


        //addDentista("Tiago Vieira", LocalDate.of(1986,03,27),"988633421","Barcelos","777656100","150600600 YZX",3);
        //getDentista(1);
        //getDados(1);

        //criarReceita("Brufen","2 comprimidos, duas vezes ao dia",2,1);
        //getReceita(1);
        //adicionarProdUsado(1,1);
        //getFatura(2);




        //addConsulta("Confirmada",LocalDateTime.of(2021,4,1,17,20), null,3,1,1,1);
        //alteraEstado(1,"Paga");

        //getProdUsado(1);

        //Consulta.getConsultasPorUtente(1);
        Conta.deleteConta(21);

        emf.close();
    }

    public static void addProcedimento(String nome, double preco, int duracao){
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;

        try{
            et = em.getTransaction();
            et.begin();

            Procedimento proced = new Procedimento();
            //proced.setIdproced(id);
            proced.setNome(nome);
            proced.setPreco(preco);
            proced.setDuracao(duracao);

            em.persist(proced);
            et.commit();
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }
    }

    public static void getProcedimento(int id) {
        EntityManager em = emf.createEntityManager();
        String query = "SELECT p FROM Procedimento p WHERE p.idproced = :id";

        TypedQuery<Procedimento> tq = em.createQuery(query, Procedimento.class);
        tq.setParameter("id", id);
        Procedimento proced = null;

        try{
            proced = tq.getSingleResult();
            System.out.println(proced.getNome() + " " + proced.getPreco());
        }catch(NoResultException ex){
            ex.printStackTrace();
        }finally {
            em.close();
        }
    }

    public static void getProcedimentos() {
        EntityManager em = emf.createEntityManager();
        String query = "SELECT p FROM Procedimento p WHERE p.idproced IS NOT NULL";
        TypedQuery<Procedimento> tq = em.createQuery(query, Procedimento.class);

        List<Procedimento> proceds;

        try{
            proceds = tq.getResultList();
            proceds.forEach(proced-> System.out.println(proced.getNome() + " " + proced.getPreco()));
        }catch(NoResultException ex){
            ex.printStackTrace();
        }finally {
            em.close();
        }
    }

    public static void changeProcedimento(int id, String nome, double preco, int duracao) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;
        Procedimento proced = null;

        try{
            et = em.getTransaction();
            et.begin();
            proced = em.find(Procedimento.class, id);

            proced.setIdproced(id);
            proced.setNome(nome);
            proced.setPreco(preco);
            proced.setDuracao(duracao);

            em.persist(proced);
            et.commit();
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }
    }

    public static void deleteProcedimento(int id) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;
        Procedimento proced = null;

        try{
            et = em.getTransaction();
            et.begin();
            proced = em.find(Procedimento.class, id);

            em.remove(proced);

            em.persist(proced);
            et.commit();
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }

        }

    public static void addProd(String nome, int quantidade){
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;

        try{
            et = em.getTransaction();
            et.begin();

            Produto proced = new Produto();
            proced.setNome(nome);
            proced.setQuantidade(quantidade);

            em.persist(proced);
            et.commit();
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }
    }


    /**
     * Obtém a o produto através da id
     * @param idprod
     * */
    public static void getProduto(int idprod) {
        EntityManager em = emf.createEntityManager();
        String query = "SELECT p FROM Produto p WHERE p.idprod = :idprod";

        TypedQuery<Produto> tq = em.createQuery(query, Produto.class);
        tq.setParameter("idprod", idprod);
        Produto prod = null;

        try{
            prod = tq.getSingleResult();
            System.out.println(prod.getNome() + " " + prod.getQuantidade());
        }catch(NoResultException ex){
            ex.printStackTrace();
        }finally {
            em.close();
        }
    }

    /**
     * Obtém a lista de produtos
     *
     * */
    public static void getListaProdutos() {
        EntityManager em = emf.createEntityManager();
        String query = "SELECT p FROM Produto p WHERE p.idprod IS NOT NULL";
        TypedQuery<Produto> tq = em.createQuery(query, Produto.class);

        List<Produto> produtos;

        try{
            produtos = tq.getResultList();
            produtos.forEach(prod -> System.out.println(prod.getNome() + " " + prod.getQuantidade()));

        }catch(NoResultException ex){
            ex.printStackTrace();
        }finally {
            em.close();
        }

    }



    /*Métodos para a entidade Conta*/

    /**
     * Insere uma nova conta na base de dados.
     * @param email
     * @param password
     * */
    public static void addConta(String email, String password){
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;

        try{
            et = em.getTransaction();
            et.begin();

            Conta conta = new Conta();


            conta.setEmail(email);
            conta.setPassword(password);

            em.persist(conta);
            et.commit();
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }
    }

    public static void getContaPorEmail(String email) {
        EntityManager em = emf.createEntityManager();
        String query = "SELECT c FROM Conta c WHERE c.email LIKE CONCAT('%', :email, '%')";

        TypedQuery<Conta> tq = em.createQuery(query, Conta.class);
        tq.setParameter("email", email);
        Conta con = null;

        try{
            con = tq.getSingleResult();
            System.out.println(con.getEmail() + " - " + con.getPassword());
        }catch(NoResultException ex){
            ex.printStackTrace();
        }finally {
            em.close();
        }
    }

    public static void getConta(int idconta) {
        EntityManager em = emf.createEntityManager();
        String query = "SELECT c FROM Conta c WHERE c.idconta = :idconta";

        TypedQuery<Conta> tq = em.createQuery(query, Conta.class);
        tq.setParameter("idconta", idconta);
        Conta conta = null;

        try{
            conta = tq.getSingleResult();
            System.out.println(conta.getEmail() + " " + conta.getPassword());
        }catch(NoResultException ex){
            ex.printStackTrace();
        }finally {
            em.close();
        }
    }

    //Já funciona - falta apanhar a excepção para quando a id não existe!
    public static void deleteConta(int idconta) {
        EntityManager em = emf.createEntityManager();
        //EntityTransaction et = null;
        Conta conta;

        try{


            conta = em.getReference(Conta.class,idconta);
            conta.getIdconta();

            /*
            et = em.getTransaction();
            et.begin();
            conta = em.find(Conta.class, idconta);*/


            em.remove(conta);
            em.getTransaction().commit();

            /*
            em.remove(conta);

            em.persist(conta);
            et.commit();*/
        }catch(Exception ex){
            if(em != null){
                //et.rollback();
                System.out.println("Esta conta não existe!");
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }

    }

    /**
     * Insere um novo funcionário na base de dados.
     * @param nome
     * @param datanascimento
     * @param contacto
     * @param morada
     * @param nif
     * @param numCC
     * @param admin
     * @param idconta
     * */
    public static void addFuncionario(String nome, LocalDate datanascimento, String contacto, String morada, String nif, String numCC, boolean admin, int idconta){
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;

        /*Procura na BD a conta com a id obtida no input*/
        String query = "SELECT c FROM Conta c WHERE c.idconta = :idconta";

        TypedQuery<Conta> tq = em.createQuery(query, Conta.class);
        tq.setParameter("idconta", idconta);
        Conta conta = null;


        try{
            et = em.getTransaction();
            et.begin();

            /*Obtém a conta com esta id*/
            conta = tq.getSingleResult();

            Funcionario func = new Funcionario();

            func.setNome(nome);
            func.setDatanascimento(datanascimento);
            func.setContacto(contacto);
            func.setNif(nif);
            func.setAdmin(admin);
            func.setNumcc(numCC);
            func.setMorada(morada);
            func.setIdconta(conta);



            em.persist(func);
            et.commit();
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }
    }

    /**
     * Obtém um funcionário com determinada ID
     * @param id
     * */
    public static void getFuncionario(int id) {
        EntityManager em = emf.createEntityManager();
        String query = "SELECT f FROM Funcionario f WHERE f.idfunc = :id";

        TypedQuery<Funcionario> tq = em.createQuery(query, Funcionario.class);
        tq.setParameter("id", id);
        Funcionario func = null;

        try{
            func = tq.getSingleResult();
            //falta adicionar info no print
            System.out.println(func.getNome() + " " + func.getDatanascimento() + " " + func.getContacto()+ " " + func.getMorada() + " " +func.getAdmin() + " " + func.getIdconta());
        }catch(NoResultException ex){
            ex.printStackTrace();
        }finally {
            em.close();
        }
    }

    /**
     * Obtém a lista de funcionários
     * */
    public static void getListaFuncionarios() {
        EntityManager em = emf.createEntityManager();
        String query = "SELECT f FROM Funcionario f WHERE f.idfunc IS NOT NULL";
        TypedQuery<Funcionario> tq = em.createQuery(query, Funcionario.class);

        List<Funcionario> funcionarios;

        try{
            funcionarios = tq.getResultList();
            funcionarios.forEach(func-> System.out.println(func.getNome() + " " + func.getDatanascimento() + " " + func.getContacto()+ " " + func.getMorada() + " " +func.getAdmin()));
        }catch(NoResultException ex){
            ex.printStackTrace();
        }finally {
            em.close();
        }
    }

    /**
     * Insere um novo utente na base de dados.
     * @param nome
     * @param datanascimento
     * @param contacto
     * @param morada
     * @param nif
     * @param numCC
     * @param historico
     * @param idconta
     * */
    public static void addUtente(String nome, LocalDate datanascimento, String contacto,String morada,String nif,String numCC, String historico, int idconta){
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;

        /*Procura na BD a conta com a id obtida no input*/
        String query = "SELECT c FROM Conta c WHERE c.idconta = :idconta";

        TypedQuery<Conta> tq = em.createQuery(query, Conta.class);
        tq.setParameter("idconta", idconta);
        Conta conta = null;


        try{
            et = em.getTransaction();
            et.begin();

            /*Obtém a conta com esta id*/
            conta = tq.getSingleResult();

            Utente ut = new Utente();
            ut.setNome(nome);
            ut.setDatanascimento(datanascimento);
            ut.setContacto(contacto);
            ut.setNif(nif);
            ut.setNumcc(numCC);
            ut.setMorada(morada);
            ut.setHistorico(historico);
            ut.setConta(conta);



            em.persist(ut);
            et.commit();
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }
    }


    /**
     * Obtém um utente com determinada ID
     * @param id
     * */
    public static void getUtente (int id) {
        EntityManager em = emf.createEntityManager();
        String query = "SELECT u FROM Utente u WHERE u.idutente = :id";

        TypedQuery<Utente> tq = em.createQuery(query, Utente.class);
        tq.setParameter("id", id);
        Utente ut = null;

        try{
            ut = tq.getSingleResult();
            //o print nao tem a info toda
            System.out.println(ut.getNome() + " " + ut.getDatanascimento() + " " + ut.getContacto()+ " " + ut.getMorada() + " " + ut.getHistorico());
        }catch(NoResultException ex){
            ex.printStackTrace();
        }finally {
            em.close();
        }
    }

    /**
     * Obtém a lista de utentes
     *
     * */
    public static void getListaUtentes() {
        EntityManager em = emf.createEntityManager();
        String query = "SELECT u FROM Utente u WHERE u.idutente IS NOT NULL";
        TypedQuery<Utente> tq = em.createQuery(query, Utente.class);

        List<Utente> utentes;

        try{
            utentes = tq.getResultList();
            utentes.forEach(ut -> System.out.println(ut.getNome() + " " + ut.getDatanascimento() + " " + ut.getContacto()+ " " + ut.getMorada() + " " + ut.getHistorico()));
        }catch(NoResultException ex){
            ex.printStackTrace();
        }finally {
            em.close();
        }

    }

    /**
     * Insere um novo dentista na base de dados.
     * @param nome
     * @param datanascimento
     * @param contacto
     * @param morada
     * @param nif
     * @param numCC
     * @param idconta
     * */
    public static void addDentista(String nome, LocalDate datanascimento, String contacto,String morada,String nif,String numCC, int idconta){
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;

        /*Procura na BD a conta com a id obtida no input*/
        String query = "SELECT c FROM Conta c WHERE c.idconta = :idconta";

        TypedQuery<Conta> tq = em.createQuery(query, Conta.class);
        tq.setParameter("idconta", idconta);
        Conta conta = null;


        try{
            et = em.getTransaction();
            et.begin();

            /*Obtém a conta com esta id*/
            conta = tq.getSingleResult();

            Dentista d = new Dentista();
            d.setNome(nome);
            d.setDatanascimento(datanascimento);
            d.setContacto(contacto);
            d.setNif(nif);
            d.setNumcc(numCC);
            d.setMorada(morada);

            d.setConta(conta);



            em.persist(d);
            et.commit();
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }
    }

    /**
     * Obtém um dentista com determinada ID
     * @param id
     * */
    public static void getDentista (int id) {
        EntityManager em = emf.createEntityManager();
        String query = "SELECT d FROM Dentista d WHERE d.iddent = :id";

        TypedQuery<Dentista> tq = em.createQuery(query, Dentista.class);
        tq.setParameter("id", id);
        Dentista d = null;

        try{
            d = tq.getSingleResult();
            //o print nao tem a info toda
            System.out.println(d.getNome() + " " + d.getDatanascimento() + " " + d.getContacto()+ " " + d.getMorada() + " " + d.getNumcc());
        }catch(NoResultException ex){
            ex.printStackTrace();
        }finally {
            em.close();
        }
    }

    /**
     * Obtém a lista de utentes
     *
     * */
    public static void getListaDentistas() {
        EntityManager em = emf.createEntityManager();
        String query = "SELECT u FROM Utente u WHERE u.idutente IS NOT NULL";
        TypedQuery<Utente> tq = em.createQuery(query, Utente.class);

        List<Utente> utentes;

        try{
            utentes = tq.getResultList();
            utentes.forEach(ut -> System.out.println(ut.getNome() + " " + ut.getDatanascimento() + " " + ut.getContacto()+ " " + ut.getMorada() + " " + ut.getHistorico()));
        }catch(NoResultException ex){
            ex.printStackTrace();
        }finally {
            em.close();
        }

    }


    /**
     * Obtém os dados da clinica
     * */
    public static void getDados (int id) {
        EntityManager em = emf.createEntityManager();
        String query = "SELECT c FROM Clinica c WHERE c.idclinica = :id";

        TypedQuery<Clinica> tq = em.createQuery(query, Clinica.class);
        tq.setParameter("id", id);
        Clinica c = null;

        try{
            c = tq.getSingleResult();
            System.out.println(c.getNome() + " " + c.getMorada() + " " + c.getNif());
        }catch(NoResultException ex){
            ex.printStackTrace();
        }finally {
            em.close();
        }
    }


    /**
     * Cria uma receita na base de dados.
     *
     * */
    public static void criarReceita(String medicacao, String descricao,int quantidade, int idconsulta){
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;

        /*Procura na BD a conta com a id obtida no input*/
        String query = "SELECT c FROM Consulta c WHERE c.idconsulta = :idconsulta";

        TypedQuery<Consulta> tq = em.createQuery(query, Consulta.class);
        tq.setParameter("idconsulta", idconsulta);
        Consulta con = null;


        try{
            et = em.getTransaction();
            et.begin();

            /*Obtém a consulta com esta id*/
            con = tq.getSingleResult();

            Receita r = new Receita();

            r.setMedicacao(medicacao);
            r.setDescricao(descricao);
            r.setQuantidade(quantidade);
            r.setIdconsulta(con);



            em.persist(r);
            et.commit();
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }
    }

    /**
     * Obtém um dentista com determinada ID
     * @param id
     * */
    public static void getReceita (int id) {
        EntityManager em = emf.createEntityManager();
        String query = "SELECT r FROM Receita r WHERE r.idreceita = :id";

        TypedQuery<Receita> tq = em.createQuery(query, Receita.class);
        tq.setParameter("id", id);
        Receita r = null;

        try{
            r = tq.getSingleResult();
            System.out.println(r.getMedicacao() + " " + r.getQuantidade() + " " + r.getDescricao() + r.getIdconsulta());
        }catch(NoResultException ex){
            ex.printStackTrace();
        }finally {
            em.close();
        }
    }


    //Já funciona!!
    public static void getProdUsado(int idconsulta){
        EntityManager em = emf.createEntityManager();
        //EntityTransaction et = null;

        String query = "SELECT p FROM ProdutoUsado p WHERE p.idconsulta.idconsulta = :idconsulta";

        TypedQuery<ProdutoUsado> tq = em.createQuery(query, ProdutoUsado.class);
        tq.setParameter("idconsulta", idconsulta);


        List<ProdutoUsado> produtos;

        try{
            produtos = tq.getResultList();
            produtos.forEach(proced -> System.out.println(proced.getQuantidade() + " " + proced.getConsulta() + " " +  proced.getProduto()));


        }catch(Exception ex){

            ex.printStackTrace();
        }finally{
            em.close();
        }
    }



    /**
     * Obtém fatura - talvez criar um novo método para ir buscar a fatura através do id da consulta, e não da sua id
     * */
    public static void getFatura (int id) {
        EntityManager em = emf.createEntityManager();
        String query = "SELECT f FROM Fatura f WHERE f.idfatura = :id";

        TypedQuery<Fatura> tq = em.createQuery(query, Fatura.class);
        tq.setParameter("id", id);
        Fatura f = null;

        try{
            f = tq.getSingleResult();
            System.out.println(f.getDataemissao()+ " " + f.getDescricao() + " " + f.getIdconsulta() + " " + f.getTotal());
        }catch(NoResultException ex){
            ex.printStackTrace();
        }finally {
            em.close();
        }
    }



    /**
     * Cria uma consulta na base de dados
     *
     * */
    public static void addConsulta(String estado, LocalDateTime datainicio, LocalDateTime datafim, int sala, int iddent, int idutente, int idproced){
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;

        /*Procura na BD a conta com a id obtida no input*/
        String queryD = "SELECT d FROM Dentista d WHERE d.iddent = :iddent";
        String queryU = "SELECT u FROM Utente u WHERE u.idutente = :idutente";
        String queryP = "SELECT p FROM Procedimento p WHERE p.idproced = :idproced";

        TypedQuery<Dentista> tq = em.createQuery(queryD, Dentista.class);
        tq.setParameter("iddent", iddent);

        TypedQuery<Utente> tqU = em.createQuery(queryU, Utente.class);
        tqU.setParameter("idutente", idutente);


        TypedQuery<Procedimento> tqP = em.createQuery(queryP, Procedimento.class);
        tqP.setParameter("idproced", idproced);



        Consulta con = null;

        Dentista d = null;
        Utente u = null;
        Procedimento p = null;


        try{
            et = em.getTransaction();
            et.begin();

            /*Obtém a consulta com esta id*/
            d = tq.getSingleResult();
            u = tqU.getSingleResult();
            p = tqP.getSingleResult();

            Consulta c = new Consulta();
            //c.setIdconsulta(idconsulta);
            c.setEstado(estado);
            c.setDatainicio(datainicio);
            c.setDatafim(datafim);
            c.setSala(sala);
            c.setIddent(d);
            c.setIdutente(u);
            c.setProcedimento(p);


            em.persist(c);
            et.commit();
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }
    }


    /**
     * Altera o estado de uma consulta
     * @param id
     * @param estado
     * */
    public static void alteraEstado(int id, String estado) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;
        Consulta con = null;

        try{
            et = em.getTransaction();
            et.begin();
            con = em.find(Consulta.class, id);


            con.setEstado(estado);

            em.persist(con);
            et.commit();
        }catch(Exception ex){
            if(et != null){
                et.rollback();
            }
            ex.printStackTrace();
        }finally{
            em.close();
        }
    }

    /**
     * fatura - talvez criar um novo método para ir buscar a fatura através do id da consulta, e não da sua id
     * */



}
