package UIForms;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import org.ipvcClinica.*;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.time.format.DateTimeFormatter;
import java.util.List;


public class FuncionarioMain extends JFrame {

    private JPanel funcMainPanel;
    private JTabbedPane tabbedPane1;
    private JPanel ConsultasTab;
    private JTable tableConsultas;
    private JComboBox comboBox;
    private JPanel UtentesTab;
    private JTable tableUtentes;
    private JTextField searchUtente;
    private JPanel ProdutosTab;
    private JTable tableProdutos;
    private JTextField searchProduto;
    private JPanel ProcedTab;
    private JTable tableProced;
    private JTextField searchProced;
    private JPanel DentistasTab;
    private JTable tableDentistas;
    private JTextField searchDentista;
    private JPanel FuncTab;
    private JButton marcarConsBtn;
    private JLabel infoFunc;
    private JButton buttonEliminarP;
    private JButton eliminarDentButton;
    private JButton permissoesButton;
    private JButton criarButton;
    private JTable tableFunc;
    private JTextField searchFunc;
    private JButton eliminarButton;
    private JButton logoutButton;
    private JPanel Procedimentos;

    public FuncionarioMain(int params) {

        super("Menu de Funcionário");
        this.setContentPane(this.funcMainPanel);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(1400, 800);
        this.setLocationRelativeTo(null);

        FuncionarioMain funcionarioMain = this;

        /**
         * Adiciona à frame as várias tabs.
         * */
        tabbedPane1.addTab("Consultas", ConsultasTab);
        tabbedPane1.addTab("Utentes", UtentesTab);
        tabbedPane1.addTab("Produtos", ProdutosTab);
        tabbedPane1.addTab("Procedimentos", ProcedTab);
        tabbedPane1.addTab("Dentistas", DentistasTab);
        tabbedPane1.addTab("Funcionários", FuncTab);

        //Desativa o tab das funções administrativas
        tabbedPane1.setEnabledAt(5, false);


        eliminarDentButton.setVisible(false);
        //criarButton.setVisible(false);


        Funcionario f = Funcionario.getFuncionarioPorConta(params);
        infoFunc.setText("Funcionário(a): " + f.getNome());

        //Permissões para funcionário Administrador
        if (f.getAdmin() == true) {

            eliminarDentButton.setVisible(true);
            //criarButton.setVisible(true);
            tabbedPane1.setEnabledAt(5, true);

        }


        /**Mostra alerta, caso o funcionáro sem permissões*/
        tabbedPane1.addComponentListener(new ComponentAdapter() {
        });
        tabbedPane1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                super.mouseClicked(e);

                if (!f.getAdmin() && (tabbedPane1.indexAtLocation(e.getX(), e.getY()) == 5)) {
                    JOptionPane.showMessageDialog(new JFrame(), "Não tem permissões para aceder!", "Alerta",
                            JOptionPane.ERROR_MESSAGE);
                }
                    /*
                    else if (tabbedPane1.indexAtLocation(e.getX(),e.getY())==4){
                        mostraDentistas();
                    } else if (tabbedPane1.indexAtLocation(e.getX(),e.getY())==1){
                        mostraUtentes();
                    }*/
            }


        });


        /**Filtros para consultas(Menu Consultas)*/
        comboBox.addItem("Todas");
        comboBox.addItem("Hoje");
        comboBox.addItem("Confirmadas");
        comboBox.addItem("Histórico");

        comboBox.setSelectedItem("Hoje");
        criaTabela();
        mostraConsultasHoje();


        comboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String selected = (String) comboBox.getSelectedItem();

                if (selected.equals("Todas")) {
                    mostraConsultas();
                } else if (selected.equals("Hoje")) {
                    mostraConsultasHoje();
                } else if (selected.equals("Confirmadas")) {
                    mostraConsultasConfirmadas();
                } else if (selected.equals("Histórico")) {
                    mostraConsultasHistorico();

                }
            }
        });


        tableConsultas.addComponentListener(new ComponentAdapter() {
        });
        tableConsultas.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);

                int index = tableConsultas.getSelectedRow();
                DefaultTableModel model = (DefaultTableModel) tableConsultas.getModel();
                int id = Integer.parseInt(model.getValueAt(index, 0).toString());

                DadosConsulta dC = new DadosConsulta(id, funcionarioMain);
                dC.setVisible(true);
            }
        });


        /**Tab Utentes*/
        criarTabelaUtentes();
        mostraUtentes();

        /*Search Bar para a Lista de utentes
         * */
        searchUtente.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {

                TableModel model = tableUtentes.getModel();
                String search = searchUtente.getText();

                TableRowSorter<TableModel> tr = new TableRowSorter<>(model);

                tableUtentes.setRowSorter(tr);
                tr.setRowFilter(RowFilter.regexFilter("(?i)" + search));

                super.keyReleased(e);
            }
        });


        tableUtentes.addComponentListener(new ComponentAdapter() {
        });
        tableUtentes.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);

                int index = tableUtentes.getSelectedRow();
                TableModel model = tableUtentes.getModel();
                int idUtente = Integer.parseInt(model.getValueAt(index, 0).toString());

                DadosUtente dU = new DadosUtente(idUtente);
                dU.setVisible(true);

            }
        });


        /*Tab Produtos*/
        criarTabelaProdutos();
        mostraProdutos();

        /*Search Bar para a lista de produtos*/
        searchProduto.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {

                TableModel model = tableProdutos.getModel();
                String search = searchProduto.getText();

                TableRowSorter<TableModel> tr = new TableRowSorter<>(model);

                tableProdutos.setRowSorter(tr);
                tr.setRowFilter(RowFilter.regexFilter("(?i)" + search));

                super.keyReleased(e);
            }
        });



        /*Tab Procedimentos*/
        criarTabelaProcedimentos();
        mostraProcedimentos();

        /*Search Bar para a lista de procedimentos*/
        searchProced.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {

                TableModel model = tableProced.getModel();
                String search = searchProced.getText();

                TableRowSorter<TableModel> tr = new TableRowSorter<>(model);

                tableProced.setRowSorter(tr);
                tr.setRowFilter(RowFilter.regexFilter("(?i)" + search));

                super.keyReleased(e);
            }
        });


        /* Remover procedimento
        buttonEliminarP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeProced(tableProced);
            }
        });*/

        eliminarDentButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removerDentista(tableDentistas);
            }
        });



        /*Tab Dentistas*/
        criarTabelaDentistas();
        mostraDentistas();

        /*Search Bar para a lista de dentistas*/
        searchDentista.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {

                TableModel model = tableDentistas.getModel();
                String search = searchDentista.getText();

                TableRowSorter<TableModel> tr = new TableRowSorter<>(model);

                tableDentistas.setRowSorter(tr);
                tr.setRowFilter(RowFilter.regexFilter("(?i)" + search));

                super.keyReleased(e);
            }
        });

        /**
         * Ao fazer um double click numa linha, abre os detalhes do funcionário selecionado.
         * */
        tableDentistas.addComponentListener(new ComponentAdapter() {
        });
        tableDentistas.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                if (f.getAdmin()) {

                    if (e.getClickCount() == 2) {

                        super.mouseClicked(e);

                        int index = tableDentistas.getSelectedRow();
                        TableModel model = tableDentistas.getModel();
                        int idDent = Integer.parseInt(model.getValueAt(index, 0).toString());

                        DadosDentista d = new DadosDentista(idDent);
                        d.setVisible(true);
                    }

                }
            }
        });

        /*Tab Funcionários*/
        criarTabelaFunc();
        mostraFunc();

        /*Search Bar para a Lista de Funcionários.
         * */
        searchFunc.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {

                TableModel model = tableFunc.getModel();
                String search = searchFunc.getText();

                TableRowSorter<TableModel> tr = new TableRowSorter<>(model);

                tableFunc.setRowSorter(tr);
                tr.setRowFilter(RowFilter.regexFilter("(?i)" + search));

                super.keyReleased(e);
            }
        });

        /**
         * Ao fazer um double click numa linha, abre os detalhes do funcionário selecionado.
         * */
        tableFunc.addComponentListener(new ComponentAdapter() {
        });
        tableFunc.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                if (e.getClickCount() == 2) {

                    super.mouseClicked(e);

                    int index = tableFunc.getSelectedRow();
                    TableModel model = tableFunc.getModel();
                    int idFunc = Integer.parseInt(model.getValueAt(index, 0).toString());

                    DetalhesFunc d = new DetalhesFunc(idFunc);
                    d.setVisible(true);
                }

            }
        });


        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeFunc(tableFunc);
            }
        });


        marcarConsBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MarcarConsulta mc = new MarcarConsulta();
                mc.setVisible(true);
            }
        });

        logoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int option = JOptionPane.showConfirmDialog(
                        funcionarioMain,
                        "Tem a certeza que pretende sair?",
                        "Logout",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (option == JOptionPane.YES_OPTION) {
                    Window win[] = Window.getWindows();
                    for (int i = 1; i < win.length; i++) {
                        win[i].dispose();
                        win[i] = null;
                    }
                }


            }
        });


        /**Permissões Administrativas*/

        criarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Registo r = new Registo(funcionarioMain, f);
                r.setVisible(true);

            }
        });


    }


    /**
     * Cria a tabela para mostrar a lista das consultas.
     */
    public void criaTabela() {

        String col[] = {"ID", "ESTADO", "DATA", "HORA INÍCIO"};

        //para que não dê para editar celulas
        TableModel model = new DefaultTableModel(null, col) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };


        tableConsultas.setModel(model);

        TableColumnModel columns = tableConsultas.getColumnModel();

        columns.getColumn(0).setPreferredWidth(10);


        /*Coloca os dados centrados na tabela*/
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment((int) JLabel.CENTER);

        columns.getColumn(0).setCellRenderer(centerRenderer);
        columns.getColumn(1).setCellRenderer(centerRenderer);
        columns.getColumn(2).setCellRenderer(centerRenderer);
        columns.getColumn(3).setCellRenderer(centerRenderer);


    }

    /**
     * Cria a tabela para mostrar a lista dos utentes.
     */
    private void criarTabelaUtentes() {

        String col[] = {"ID", "NOME", "CONTACTO"};

        //para que não dê para editar celulas
        TableModel model = new DefaultTableModel(null, col) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tableUtentes.setModel(model);

        TableColumnModel columns = tableUtentes.getColumnModel();


        columns.getColumn(0).setPreferredWidth(10);


        //Coloca os dados centrados na tabela
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment((int) JLabel.CENTER);

        columns.getColumn(0).setCellRenderer(centerRenderer);
        columns.getColumn(1).setCellRenderer(centerRenderer);
        columns.getColumn(2).setCellRenderer(centerRenderer);


    }

    /**
     * Cria a tabela para mostrar a lista dos produtos.
     */
    private void criarTabelaProdutos() {

        String col[] = {"ID", "NOME", "QUANTIDADE"};

        //para que não dê para editar celulas
        TableModel model = new DefaultTableModel(null, col) {
            public boolean isCellEditable(int row, int column) {

                switch (column) {
                    case 2:
                        return true;
                    default:
                        return false;
                }
            }
        };


        tableProdutos.setModel(model);

        /*Altera o stock de um determinado produto ao alterar o seu valor na tabela dos produtos */
        tableProdutos.getModel().addTableModelListener(new TableModelListener() {

            public void tableChanged(TableModelEvent e) {


                int quantidade;

                if (e.getType() == TableModelEvent.UPDATE) {
                    int col = tableProdutos.getSelectedColumn();
                    int row = tableProdutos.getSelectedRow();

                    /*Obtém a ID do produto*/
                    int id = Integer.parseInt((String) tableProdutos.getValueAt(row, 0));

                    quantidade = Integer.parseInt((String) tableProdutos.getValueAt(row, col));

                    Produto.alteraStock(id, quantidade);

                    //System.out.println(quantidade + " ID: " + id);
                }


            }
        });

        TableColumnModel columns = tableProdutos.getColumnModel();

        columns.getColumn(0).setPreferredWidth(10);

        //Coloca os dados centrados na tabela
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment((int) JLabel.CENTER);

        columns.getColumn(0).setCellRenderer(centerRenderer);
        columns.getColumn(1).setCellRenderer(centerRenderer);
        columns.getColumn(2).setCellRenderer(centerRenderer);

    }

    /**
     * Cria a tabela para mostrar a lista dos procedimentos.
     */
    private void criarTabelaProcedimentos() {

        String col[] = {"ID", "NOME", "PREÇO", "DURAÇÃO"};

        //para que não dê para editar celulas
        TableModel model = new DefaultTableModel(null, col) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tableProced.setModel(model);

        TableColumnModel columns = tableProced.getColumnModel();

        columns.getColumn(0).setPreferredWidth(10);

        //Coloca os dados centrados na tabela
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment((int) JLabel.CENTER);

        columns.getColumn(0).setCellRenderer(centerRenderer);
        columns.getColumn(1).setCellRenderer(centerRenderer);
        columns.getColumn(2).setCellRenderer(centerRenderer);
        columns.getColumn(3).setCellRenderer(centerRenderer);


    }

    /**
     * Cria a tabela para mostrar a lista dos dentistas.
     */
    private void criarTabelaDentistas() {

        String col[] = {"ID", "NOME", "CONTACTO"};

        //para que não dê para editar celulas
        TableModel model = new DefaultTableModel(null, col) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tableDentistas.setModel(model);

        TableColumnModel columns = tableDentistas.getColumnModel();

        columns.getColumn(0).setPreferredWidth(10);

        //Coloca os dados centrados na tabela
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment((int) JLabel.CENTER);

        columns.getColumn(0).setCellRenderer(centerRenderer);
        columns.getColumn(1).setCellRenderer(centerRenderer);
        columns.getColumn(2).setCellRenderer(centerRenderer);

    }


    /**
     * Insere na tabela a lista das consultas provenientes da BD.
     */
    public void mostraConsultas() {

        List<Consulta> consultas = Consulta.getListaConsultas();
        DefaultTableModel model = (DefaultTableModel) tableConsultas.getModel();

        DateTimeFormatter formatterHoras = DateTimeFormatter.ofPattern("HH:mm");
        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        model.setRowCount(0);


        for (Consulta c : consultas) {

            model.addRow(new Object[]{String.valueOf(c.getIdconsulta()), c.getEstado(), formatterData.format(c.getDatainicio()), formatterHoras.format(c.getDatainicio()) + "h"});
        }
    }

    /**
     * Insere na tabela todas as consultas como estado seja "Confirmada".
     */
    public void mostraConsultasConfirmadas() {

        List<Consulta> consultas = Consulta.getConsultasConfirmadas();
        DefaultTableModel model = (DefaultTableModel) tableConsultas.getModel();


        DateTimeFormatter formatterHoras = DateTimeFormatter.ofPattern("HH:mm");
        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        model.setRowCount(0);

        for (Consulta c : consultas) {

            model.addRow(new Object[]{String.valueOf(c.getIdconsulta()), c.getEstado(), formatterData.format(c.getDatainicio()), formatterHoras.format(c.getDatainicio()) + "h"});
        }
    }

    /**
     * Insere na tabela ".
     */
    public void mostraConsultasHoje() {

        List<Consulta> consultas = Consulta.getConsultasHoje();
        DefaultTableModel model = (DefaultTableModel) tableConsultas.getModel();


        DateTimeFormatter formatterHoras = DateTimeFormatter.ofPattern("HH:mm");
        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        model.setRowCount(0);

        for (Consulta c : consultas) {

            model.addRow(new Object[]{String.valueOf(c.getIdconsulta()), c.getEstado(), formatterData.format(c.getDatainicio()), formatterHoras.format(c.getDatainicio()) + "h"});
        }
    }

    /**
     * Insere na tabela todas as consultas como estado seja "Confirmada".
     */
    public void mostraConsultasHistorico() {

        List<Consulta> consultas = Consulta.getConsultasHistorico();
        DefaultTableModel model = (DefaultTableModel) tableConsultas.getModel();


        DateTimeFormatter formatterHoras = DateTimeFormatter.ofPattern("HH:mm");
        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        model.setRowCount(0);

        for (Consulta c : consultas) {

            model.addRow(new Object[]{String.valueOf(c.getIdconsulta()), c.getEstado(), formatterData.format(c.getDatainicio()), formatterHoras.format(c.getDatainicio()) + "h"});
        }
    }


    /**
     * Insere na tabela a lista dos utentes provenientes da BD.
     */
    public void mostraUtentes() {


        List<Utente> utentes = Utente.getListaUtentes();
        DefaultTableModel model = (DefaultTableModel) tableUtentes.getModel();


        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        model.setRowCount(0);

        for (Utente u : utentes) {
            model.addRow(new Object[]{String.valueOf(u.getIdutente()), u.getNome(), u.getContacto()});
        }
    }

    /**
     * Insere na tabela a lista dos produtos provenientes da BD.
     */
    private void mostraProdutos() {

        List<Produto> produtos = Produto.getListaProdutos();
        DefaultTableModel model = (DefaultTableModel) tableProdutos.getModel();

        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        model.setRowCount(0);

        for (Produto p : produtos) {
            model.addRow(new Object[]{String.valueOf(p.getIdprod()), p.getNome(), String.valueOf(p.getQuantidade())});
        }
    }

    /**
     * Insere na tabela a lista dos procedimentos provenientes da BD.
     */
    private void mostraProcedimentos() {

        List<Procedimento> proceds = Procedimento.getListaProcedimentos();
        DefaultTableModel model = (DefaultTableModel) tableProced.getModel();


        model.setRowCount(0);

        for (Procedimento p : proceds) {
            model.addRow(new Object[]{String.valueOf(p.getIdproced()), p.getNome(), p.getPreco() + "€", p.getDuracao() + "h"});
        }
    }

    /**
     * Insere na tabela a lista dos dentistas provenientes da BD.
     */
    public void mostraDentistas() {

        List<Dentista> dentistas = Dentista.getListaDentistas();
        DefaultTableModel model = (DefaultTableModel) tableDentistas.getModel();


        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        model.setRowCount(0);

        for (Dentista d : dentistas) {
            model.addRow(new Object[]{String.valueOf(d.getIddent()), d.getNome(), d.getContacto()});
        }
    }


    /**
     * Remove um procedimento do sistema.
     * @param table
     * */
    /*
    public void removeProced (JTable table){
        DefaultTableModel model = (DefaultTableModel) tableProced.getModel();
        int[] rows = table.getSelectedRows();


        for(int i=0;i<rows.length;i++) {
            int row = rows[i] - i;
            int idProc = Integer.parseInt((String) table.getValueAt(rows[i] - i, 0));

            //Alterar o alerta ou remover
            if (Procedimento.deleteProced(idProc) == true){
                model.removeRow(rows[i] - i);
            System.out.println("Row: " + row + " idProcedimento: " + idProc);
            } else {
                JOptionPane.showMessageDialog(null, "O Procedimento está associado a uma consulta!"); //not working
            }
        }
    }*/

    /**
     * Remove o dentista do sistema.
     *
     * @param table
     */
    public void removerDentista(JTable table) {
        DefaultTableModel model = (DefaultTableModel) tableDentistas.getModel();
        int[] rows = table.getSelectedRows();

        boolean removido = false;

        for (int i = 0; i < rows.length; i++) {
            int row = rows[i] - i;
            //Obter id da conta deste dentista
            int idDent = Integer.parseInt((String) table.getValueAt(rows[i] - i, 0));
            Dentista d = Dentista.getDentista(idDent);
            Conta c = Conta.getConta(d.getConta().getIdconta());

            System.out.println(idDent + d.getNome() + c.getIdconta() + "teste");

            //Só remove da tabela se o método na BD removeu com sucesso
            if (Conta.deleteConta(c.getIdconta()) == true) {
                model.removeRow(rows[i] - i);
                removido = true;
                System.out.println("Row: " + row + " idDentista: " + idDent);
            }
        }

        //Esta variável evita mostrar o alerta por cada remoção de dentista
        if (removido) {
            JOptionPane.showMessageDialog(new JFrame(), "Dentista(s) removido(s) com sucesso!", "Sucesso",
                    JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(new JFrame(), "Ocorreu um erro ao eliminar o(s) dentista(s)!", "Erro",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Cria a tabela para mostrar a lista dos utentes.
     */
    private void criarTabelaFunc() {

        String col[] = {"ID", "NOME", "CONTACTO"};

        //para que não dê para editar celulas
        TableModel model = new DefaultTableModel(null, col) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tableFunc.setModel(model);

        TableColumnModel columns = tableFunc.getColumnModel();


        columns.getColumn(0).setPreferredWidth(10);


        //Coloca os dados centrados na tabela
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment((int) JLabel.CENTER);

        columns.getColumn(0).setCellRenderer(centerRenderer);
        columns.getColumn(1).setCellRenderer(centerRenderer);
        columns.getColumn(2).setCellRenderer(centerRenderer);


    }

    /**
     * Insere na tabela a lista dos funcionários provenientes da BD.
     */
    public void mostraFunc() {

        List<Funcionario> funcionarios = Funcionario.getListaFunc();
        DefaultTableModel model = (DefaultTableModel) tableFunc.getModel();


        model.setRowCount(0);

        for (Funcionario f : funcionarios) {
            model.addRow(new Object[]{String.valueOf(f.getIdfunc()), f.getNome(), f.getContacto()});
        }
    }

    /**
     * Remove um funcionario do sistema.
     *
     * @param table
     */
    public void removeFunc(JTable table) {
        DefaultTableModel model = (DefaultTableModel) tableFunc.getModel();
        int[] rows = table.getSelectedRows();

        boolean removido = false;

        for (int i = 0; i < rows.length; i++) {
            int row = rows[i] - i;
            int idFuncionario = Integer.parseInt((String) table.getValueAt(rows[i] - i, 0));

            //Só remove da tabela se o método na BD removeu com sucesso(remove também a conta por causa de ser FK on cascade)
            if (Funcionario.deleteFunc(idFuncionario) == true) {
                model.removeRow(rows[i] - i);
                removido = true;
                System.out.println("Row: " + row + " idFuncionario: " + idFuncionario);
            }
        }

        //Esta variável evita mostrar o alerta por cada remoção de funcionário
        if (removido) {
            JOptionPane.showMessageDialog(null, "Funcionário(s) removido(s) com sucesso!");
        } else {
            JOptionPane.showMessageDialog(null, "Ocorreu um erro ao eliminar o funcionário!");
        }
    }


    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    public static void main(String[] args) {

        //FuncionarioMain listaConsultas = new FuncionarioMain();
        //listaConsultas.setVisible(true);
    }


    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        funcMainPanel = new JPanel();
        funcMainPanel.setLayout(new GridLayoutManager(3, 2, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane1 = new JTabbedPane();
        funcMainPanel.add(tabbedPane1, new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(200, 200), null, 0, false));
        ConsultasTab = new JPanel();
        ConsultasTab.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane1.addTab("Untitled", ConsultasTab);
        final JScrollPane scrollPane1 = new JScrollPane();
        ConsultasTab.add(scrollPane1, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        tableConsultas = new JTable();
        scrollPane1.setViewportView(tableConsultas);
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        ConsultasTab.add(panel1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label1 = new JLabel();
        Font label1Font = this.$$$getFont$$$("Ayuthaya", -1, 18, label1.getFont());
        if (label1Font != null) label1.setFont(label1Font);
        label1.setText("Consultas");
        panel1.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        ConsultasTab.add(panel2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        panel2.add(panel3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        marcarConsBtn = new JButton();
        marcarConsBtn.setText("Adicionar");
        panel3.add(marcarConsBtn, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        panel3.add(spacer1, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final JPanel panel4 = new JPanel();
        panel4.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel2.add(panel4, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        comboBox = new JComboBox();
        panel4.add(comboBox, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        UtentesTab = new JPanel();
        UtentesTab.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane1.addTab("Untitled", UtentesTab);
        final JScrollPane scrollPane2 = new JScrollPane();
        UtentesTab.add(scrollPane2, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        tableUtentes = new JTable();
        scrollPane2.setViewportView(tableUtentes);
        final JPanel panel5 = new JPanel();
        panel5.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        UtentesTab.add(panel5, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel6 = new JPanel();
        panel6.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel5.add(panel6, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        Font label2Font = this.$$$getFont$$$("Arial", Font.PLAIN, 14, label2.getFont());
        if (label2Font != null) label2.setFont(label2Font);
        label2.setHorizontalAlignment(11);
        label2.setHorizontalTextPosition(11);
        label2.setText("Procurar:");
        panel6.add(label2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel7 = new JPanel();
        panel7.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel5.add(panel7, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        searchUtente = new JTextField();
        panel7.add(searchUtente, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final JPanel panel8 = new JPanel();
        panel8.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        UtentesTab.add(panel8, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label3 = new JLabel();
        Font label3Font = this.$$$getFont$$$("Arial Black", -1, 18, label3.getFont());
        if (label3Font != null) label3.setFont(label3Font);
        label3.setText("Utentes");
        panel8.add(label3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        ProdutosTab = new JPanel();
        ProdutosTab.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane1.addTab("Untitled", ProdutosTab);
        final JScrollPane scrollPane3 = new JScrollPane();
        ProdutosTab.add(scrollPane3, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        tableProdutos = new JTable();
        scrollPane3.setViewportView(tableProdutos);
        final JPanel panel9 = new JPanel();
        panel9.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        ProdutosTab.add(panel9, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label4 = new JLabel();
        Font label4Font = this.$$$getFont$$$("Arial Black", -1, 18, label4.getFont());
        if (label4Font != null) label4.setFont(label4Font);
        label4.setText("Produtos");
        panel9.add(label4, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel10 = new JPanel();
        panel10.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        ProdutosTab.add(panel10, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel11 = new JPanel();
        panel11.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel10.add(panel11, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label5 = new JLabel();
        Font label5Font = this.$$$getFont$$$("Arial", -1, 14, label5.getFont());
        if (label5Font != null) label5.setFont(label5Font);
        label5.setText("Procurar:");
        panel11.add(label5, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel12 = new JPanel();
        panel12.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel10.add(panel12, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        searchProduto = new JTextField();
        panel12.add(searchProduto, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        ProcedTab = new JPanel();
        ProcedTab.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane1.addTab("Untitled", ProcedTab);
        final JScrollPane scrollPane4 = new JScrollPane();
        ProcedTab.add(scrollPane4, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        tableProced = new JTable();
        scrollPane4.setViewportView(tableProced);
        final JPanel panel13 = new JPanel();
        panel13.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        ProcedTab.add(panel13, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label6 = new JLabel();
        Font label6Font = this.$$$getFont$$$("Arial Black", -1, 18, label6.getFont());
        if (label6Font != null) label6.setFont(label6Font);
        label6.setText("Procedimentos");
        panel13.add(label6, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel14 = new JPanel();
        panel14.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        ProcedTab.add(panel14, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel15 = new JPanel();
        panel15.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel14.add(panel15, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label7 = new JLabel();
        Font label7Font = this.$$$getFont$$$("Arial", -1, 14, label7.getFont());
        if (label7Font != null) label7.setFont(label7Font);
        label7.setText("Procurar:");
        panel15.add(label7, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel16 = new JPanel();
        panel16.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel14.add(panel16, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        searchProced = new JTextField();
        panel16.add(searchProced, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        DentistasTab = new JPanel();
        DentistasTab.setLayout(new GridLayoutManager(4, 1, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane1.addTab("Untitled", DentistasTab);
        final JScrollPane scrollPane5 = new JScrollPane();
        DentistasTab.add(scrollPane5, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        tableDentistas = new JTable();
        scrollPane5.setViewportView(tableDentistas);
        final JPanel panel17 = new JPanel();
        panel17.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        DentistasTab.add(panel17, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label8 = new JLabel();
        Font label8Font = this.$$$getFont$$$("Arial Black", -1, 18, label8.getFont());
        if (label8Font != null) label8.setFont(label8Font);
        label8.setText("Dentistas");
        panel17.add(label8, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel18 = new JPanel();
        panel18.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        DentistasTab.add(panel18, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel19 = new JPanel();
        panel19.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel18.add(panel19, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label9 = new JLabel();
        Font label9Font = this.$$$getFont$$$("Arial", -1, 14, label9.getFont());
        if (label9Font != null) label9.setFont(label9Font);
        label9.setText("Procurar:");
        panel19.add(label9, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel20 = new JPanel();
        panel20.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel18.add(panel20, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        searchDentista = new JTextField();
        panel20.add(searchDentista, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        eliminarDentButton = new JButton();
        eliminarDentButton.setText("Eliminar");
        DentistasTab.add(eliminarDentButton, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        FuncTab = new JPanel();
        FuncTab.setLayout(new GridLayoutManager(4, 2, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane1.addTab("Untitled", FuncTab);
        final JScrollPane scrollPane6 = new JScrollPane();
        FuncTab.add(scrollPane6, new GridConstraints(2, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        tableFunc = new JTable();
        scrollPane6.setViewportView(tableFunc);
        final JPanel panel21 = new JPanel();
        panel21.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        FuncTab.add(panel21, new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label10 = new JLabel();
        Font label10Font = this.$$$getFont$$$("Arial Black", -1, 18, label10.getFont());
        if (label10Font != null) label10.setFont(label10Font);
        label10.setText("Funcionários");
        panel21.add(label10, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel22 = new JPanel();
        panel22.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        FuncTab.add(panel22, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label11 = new JLabel();
        Font label11Font = this.$$$getFont$$$("Arial", -1, 14, label11.getFont());
        if (label11Font != null) label11.setFont(label11Font);
        label11.setText("Procurar:");
        panel22.add(label11, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel23 = new JPanel();
        panel23.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        FuncTab.add(panel23, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        searchFunc = new JTextField();
        panel23.add(searchFunc, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        eliminarButton = new JButton();
        eliminarButton.setText("Eliminar");
        FuncTab.add(eliminarButton, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        infoFunc = new JLabel();
        Font infoFuncFont = this.$$$getFont$$$("Arial", -1, 18, infoFunc.getFont());
        if (infoFuncFont != null) infoFunc.setFont(infoFuncFont);
        infoFunc.setText("infoFunc");
        funcMainPanel.add(infoFunc, new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        criarButton = new JButton();
        criarButton.setText("Criar conta");
        funcMainPanel.add(criarButton, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        logoutButton = new JButton();
        logoutButton.setText("Logout");
        funcMainPanel.add(logoutButton, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return funcMainPanel;
    }

}
