package UIForms;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import org.ipvcClinica.Conta;
import org.ipvcClinica.Dentista;
import org.ipvcClinica.Funcionario;
import org.ipvcClinica.Utente;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.chrono.ChronoLocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;


public class Registo extends JFrame implements PropertyChangeListener, ActionListener {


    private JPanel registoPanel;
    private JRadioButton dentistaRadioButton;
    private JRadioButton utenteRadioButton;
    private JRadioButton funcRadioButton;
    private JPasswordField password;
    private JTextField nome;
    private JTextField contacto;
    private JTextField nif;
    private JTextField nCC;
    private JTextField morada;
    private JTextField email;
    private JButton registarButton;
    private JButton button1;
    private LocalDateTime currentDate;
    private JFormattedTextField datePickerTxt;
    private JButton limparButton;

    private LocalDate data;
    private String nomeR;
    private String contactoR;
    private String nifR;
    private String nCCr;
    private String moradaR;
    private String emailR;
    private String passwordR;

    private LocalDate dataNasc;


    public Registo(FuncionarioMain funcionarioMain, Funcionario funcionario) {

        super("Registo");
        this.setContentPane(this.registoPanel);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(900, 700);
        this.setLocationRelativeTo(null);


        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        currentDate = LocalDateTime.now();


        datePickerTxt.setValue(formatterData.format(currentDate));

        Calendario cal = new Calendario();
        cal.setUndecorated(true);
        cal.addPropertyChangeListener(this);

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cal.setLocation(datePickerTxt.getLocationOnScreen().x,
                        (datePickerTxt.getLocationOnScreen().y + datePickerTxt.getHeight()));

                cal.setVisible(true);
            }
        });



        /*Radio buttons*/
        ButtonGroup group = new ButtonGroup();
        group.add(dentistaRadioButton);
        group.add(utenteRadioButton);
        group.add(funcRadioButton);

        dentistaRadioButton.addActionListener(this);
        utenteRadioButton.addActionListener(this);
        funcRadioButton.addActionListener(this);

        if (funcionario.getAdmin() == false) {
            dentistaRadioButton.setEnabled(false);
            funcRadioButton.setEnabled(false);
        }


        /*Cria regista uma nova conta e utilizador no sistema.*/
        registarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                /*Obtém os valores dos campos introduzidos pelo utilizador.*/
                nomeR = nome.getText();
                contactoR = contacto.getText();
                nifR = nif.getText();
                moradaR = morada.getText();
                emailR = email.getText();
                passwordR = password.getText();
                nCCr = nCC.getText();




                /*Obtém o valor da data de Nascimento e formata-o para uma variável LocalDate*/
                DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                dataNasc = LocalDate.parse(datePickerTxt.getValue().toString(), formatterData);


                if (nomeR.isEmpty() || contactoR.isEmpty() || nifR.isEmpty() || moradaR.isEmpty() || emailR.isEmpty() || passwordR.isEmpty() || nCCr.isEmpty()) {
                    JOptionPane.showMessageDialog(new JFrame(), "Campos vazios não permitidos!", "Erro",
                            JOptionPane.ERROR_MESSAGE);
                }
                //Não deixa inserir com data de nascimento igual à data atual ou após a data
                else if (dataNasc.equals(currentDate.toLocalDate()) || dataNasc.isAfter(ChronoLocalDate.from(currentDate))) {
                    JOptionPane.showMessageDialog(new JFrame(), "Data de nascimento não válida!", "Erro",
                            JOptionPane.ERROR_MESSAGE);
                } else {

                    if (Conta.verificaEmail(emailR)) {

                        JOptionPane.showMessageDialog(new JFrame(), "Conta com esse email já existe!", "Erro",
                                JOptionPane.ERROR_MESSAGE);
                    } else {

                        if (dentistaRadioButton.isSelected()) {

                            Conta.addConta(emailR, passwordR, "dentista");

                            Conta c = Conta.getContaPorCredenciais(emailR);

                            Dentista.addDentista(nomeR, dataNasc, contactoR, moradaR, nifR, nCCr, c.getIdconta());

                            JOptionPane.showMessageDialog(null, "Conta de Dentista criada com sucesso!");

                            limpaCampos();
                            funcionarioMain.mostraDentistas();

                        } else if (utenteRadioButton.isSelected()) {

                            Conta.addConta(emailR, passwordR, "utente");
                            Conta c = Conta.getContaPorCredenciais(emailR);

                            /*Insere com o histórico vazio por defeito*/
                            Utente.addUtente(nomeR, dataNasc, contactoR, moradaR, nifR, nCCr, "", c.getIdconta());

                            JOptionPane.showMessageDialog(null, "Conta de Utente criada com sucesso!");

                            limpaCampos();
                            funcionarioMain.mostraUtentes();

                        } else if (funcRadioButton.isSelected()) {
                            Conta.addConta(emailR, passwordR, "funcionario");

                            Conta c = Conta.getContaPorCredenciais(emailR);

                            /*Insere com sem permissões de administrador por defeito*/
                            Funcionario.addFuncionario(nomeR, dataNasc, contactoR, moradaR, nifR, nCCr, false, c.getIdconta());

                            JOptionPane.showMessageDialog(null, "Conta de Funcionário criada com sucesso!");

                            limpaCampos();
                            funcionarioMain.mostraFunc();

                        } else {


                            JOptionPane.showMessageDialog(new JFrame(), "Seleciona um tipo de utilizador!", "Erro",
                                    JOptionPane.ERROR_MESSAGE);


                        }
                    }

                }
            }
        });

        /**
         * Ao clicar no botão "Limpar" limpa todos campos do registo.
         * */
        limparButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                limpaCampos();
            }
        });

    }

    /**
     * Gera as ações dos Radio Button(Verifica se não faz falta?)
     */
    public void actionPerformed(ActionEvent e) {


        if (e.getSource() == dentistaRadioButton) {
            System.out.print("DEntista\n");
        } else if (e.getSource() == utenteRadioButton) {
            System.out.print("Utente\n");
        } else if (e.getSource() == funcRadioButton) {
            System.out.print("Funcionário\n");
        }

    }


    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        if (evt.getPropertyName().equals("selectedDate")) {
            Calendar cal = (Calendar) evt.getNewValue();
            data = cal.getTime().toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();

            datePickerTxt.setValue(formatterData.format(data));


        }


    }

    /**
     * Limpa todos os campos, e restaura a data para a data atual
     */
    public void limpaCampos() {

        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        nome.setText("");
        contacto.setText("");
        nif.setText("");
        nCC.setText("");
        morada.setText("");
        email.setText("");
        password.setText("");

        datePickerTxt.setValue(formatterData.format(LocalDate.now()));

    }

    public static void main(String[] args) {

    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        registoPanel = new JPanel();
        registoPanel.setLayout(new GridLayoutManager(23, 19, new Insets(0, 0, 0, 0), -1, -1));
        final Spacer spacer1 = new Spacer();
        registoPanel.add(spacer1, new GridConstraints(3, 18, 11, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final JLabel label1 = new JLabel();
        Font label1Font = this.$$$getFont$$$(null, Font.BOLD, -1, label1.getFont());
        if (label1Font != null) label1.setFont(label1Font);
        label1.setText("Tipo de Conta:");
        registoPanel.add(label1, new GridConstraints(21, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer2 = new Spacer();
        registoPanel.add(spacer2, new GridConstraints(19, 16, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final Spacer spacer3 = new Spacer();
        registoPanel.add(spacer3, new GridConstraints(20, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final Spacer spacer4 = new Spacer();
        registoPanel.add(spacer4, new GridConstraints(17, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        password = new JPasswordField();
        registoPanel.add(password, new GridConstraints(18, 3, 1, 5, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final JLabel label2 = new JLabel();
        Font label2Font = this.$$$getFont$$$(null, Font.BOLD, -1, label2.getFont());
        if (label2Font != null) label2.setFont(label2Font);
        label2.setText("Password:");
        registoPanel.add(label2, new GridConstraints(18, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer5 = new Spacer();
        registoPanel.add(spacer5, new GridConstraints(15, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final Spacer spacer6 = new Spacer();
        registoPanel.add(spacer6, new GridConstraints(14, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final Spacer spacer7 = new Spacer();
        registoPanel.add(spacer7, new GridConstraints(8, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final Spacer spacer8 = new Spacer();
        registoPanel.add(spacer8, new GridConstraints(6, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final Spacer spacer9 = new Spacer();
        registoPanel.add(spacer9, new GridConstraints(4, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        nome = new JTextField();
        registoPanel.add(nome, new GridConstraints(4, 3, 1, 5, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final JLabel label3 = new JLabel();
        Font label3Font = this.$$$getFont$$$(null, Font.BOLD, -1, label3.getFont());
        if (label3Font != null) label3.setFont(label3Font);
        label3.setText("Nome:");
        registoPanel.add(label3, new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label4 = new JLabel();
        Font label4Font = this.$$$getFont$$$(null, Font.BOLD, -1, label4.getFont());
        if (label4Font != null) label4.setFont(label4Font);
        label4.setText("Data de Nascimento:");
        registoPanel.add(label4, new GridConstraints(6, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label5 = new JLabel();
        Font label5Font = this.$$$getFont$$$(null, Font.BOLD, -1, label5.getFont());
        if (label5Font != null) label5.setFont(label5Font);
        label5.setText("Contacto:");
        registoPanel.add(label5, new GridConstraints(8, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        contacto = new JTextField();
        registoPanel.add(contacto, new GridConstraints(8, 3, 1, 5, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        nif = new JTextField();
        nif.setText("");
        registoPanel.add(nif, new GridConstraints(10, 3, 1, 5, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final JLabel label6 = new JLabel();
        Font label6Font = this.$$$getFont$$$(null, Font.BOLD, -1, label6.getFont());
        if (label6Font != null) label6.setFont(label6Font);
        label6.setText("NIF:");
        registoPanel.add(label6, new GridConstraints(10, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        nCC = new JTextField();
        nCC.setText("");
        registoPanel.add(nCC, new GridConstraints(12, 3, 1, 5, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final JLabel label7 = new JLabel();
        Font label7Font = this.$$$getFont$$$(null, Font.BOLD, -1, label7.getFont());
        if (label7Font != null) label7.setFont(label7Font);
        label7.setText("Cartão de Cidadão:");
        registoPanel.add(label7, new GridConstraints(12, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label8 = new JLabel();
        Font label8Font = this.$$$getFont$$$(null, Font.BOLD, -1, label8.getFont());
        if (label8Font != null) label8.setFont(label8Font);
        label8.setText("Morada:");
        registoPanel.add(label8, new GridConstraints(14, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        morada = new JTextField();
        morada.setText("");
        registoPanel.add(morada, new GridConstraints(14, 3, 1, 5, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        email = new JTextField();
        email.setText("");
        registoPanel.add(email, new GridConstraints(16, 3, 1, 5, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final JLabel label9 = new JLabel();
        Font label9Font = this.$$$getFont$$$(null, Font.BOLD, -1, label9.getFont());
        if (label9Font != null) label9.setFont(label9Font);
        label9.setText("Email:");
        registoPanel.add(label9, new GridConstraints(16, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        funcRadioButton = new JRadioButton();
        funcRadioButton.setText("Funcionário");
        registoPanel.add(funcRadioButton, new GridConstraints(21, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer10 = new Spacer();
        registoPanel.add(spacer10, new GridConstraints(22, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        datePickerTxt = new JFormattedTextField();
        registoPanel.add(datePickerTxt, new GridConstraints(6, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final Spacer spacer11 = new Spacer();
        registoPanel.add(spacer11, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        button1 = new JButton();
        button1.setText("Selecionar Data");
        registoPanel.add(button1, new GridConstraints(6, 4, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer12 = new Spacer();
        registoPanel.add(spacer12, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        dentistaRadioButton = new JRadioButton();
        dentistaRadioButton.setText("Dentista");
        registoPanel.add(dentistaRadioButton, new GridConstraints(21, 4, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        utenteRadioButton = new JRadioButton();
        utenteRadioButton.setText("Utente");
        registoPanel.add(utenteRadioButton, new GridConstraints(21, 5, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        registarButton = new JButton();
        Font registarButtonFont = this.$$$getFont$$$("Arial", -1, 14, registarButton.getFont());
        if (registarButtonFont != null) registarButton.setFont(registarButtonFont);
        registarButton.setText("Registar");
        registoPanel.add(registarButton, new GridConstraints(22, 6, 1, 5, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label10 = new JLabel();
        Font label10Font = this.$$$getFont$$$("Arial", Font.BOLD, 18, label10.getFont());
        if (label10Font != null) label10.setFont(label10Font);
        label10.setText("Registo");
        registoPanel.add(label10, new GridConstraints(1, 1, 1, 18, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer13 = new Spacer();
        registoPanel.add(spacer13, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        limparButton = new JButton();
        limparButton.setText("Limpar");
        registoPanel.add(limparButton, new GridConstraints(22, 11, 1, 5, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return registoPanel;
    }

}
