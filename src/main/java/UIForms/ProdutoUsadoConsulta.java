package UIForms;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import org.ipvcClinica.Consulta;
import org.ipvcClinica.Produto;
import org.ipvcClinica.ProdutoUsado;
import org.ipvcClinica.Receita;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ProdutoUsadoConsulta extends JFrame {
    private JPanel prodsUsadosPnl;
    private JButton adicionarBtn;
    private JButton removerBtn;
    private JSpinner qtdSpinner;
    private JTable prodsTbl;
    private JTable prodsUsadosTbl;
    private JScrollPane puScroll;
    private JScrollPane pScroll;

    public ProdutoUsadoConsulta(int idConsulta) {
        super("Produtos Usados na Consulta");
        this.setContentPane(this.prodsUsadosPnl);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(900, 600);
        this.setLocationRelativeTo(null);

        pScroll.setPreferredSize(pScroll.getPreferredSize());
        puScroll.setPreferredSize(puScroll.getPreferredSize());

        SpinnerModel modeltau = new SpinnerNumberModel(1, 1, 20, 1);
        qtdSpinner.setModel(modeltau);

        criarTabelas(idConsulta);

        List<Produto> produtos = Produto.getListaProdutos();
        DefaultTableModel model1 = (DefaultTableModel) prodsTbl.getModel();

        model1.setRowCount(0);

        for (Produto p : produtos) {
            model1.addRow(new Object[]{p.getNome(), String.valueOf(p.getQuantidade())
            });
        }

        List<ProdutoUsado> produtosUsados = ProdutoUsado.getListaProdsUsados(idConsulta);
        DefaultTableModel model2 = (DefaultTableModel) prodsUsadosTbl.getModel();

        model2.setRowCount(0);

        for (ProdutoUsado pu : produtosUsados) {
            model2.addRow(new Object[]{pu.getProduto().getNome(), String.valueOf(pu.getQuantidade())
            });
        }

        Object[] rowProdUsado = new Object[2];
        adicionarBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedProd = prodsTbl.getSelectedRow();

                if (selectedProd >= 0) {

                    //iterar sobre a lista para saber se produto já está usado
                    int i = 0;

                    for (ProdutoUsado pu : produtosUsados) {
                        if (pu.getProduto().getIdprod() == produtos.get(selectedProd).getIdprod()) {
                            i = 1;
                            System.out.println("produto já usado");
                        }
                    }

                    //diferenciar entre produtos
                    ProdutoUsado puAdicionado = null; //nao é preciso associar a variavel mas deixo assim se no futuro conseguir por a dar como quero ;-;
                    if (i != 0) {
                        puAdicionado = ProdutoUsado.updateProdutoUsado(Consulta.getConsulta(idConsulta).getIdconsulta(), produtos.get(selectedProd).getIdprod(), String.valueOf(qtdSpinner.getValue()));
                        //model2.removeRow(i);
                        //produtosUsados.set(i, puAdicionado);
                    } else {
                        puAdicionado = ProdutoUsado.addProdUsado(Consulta.getConsulta(idConsulta), produtos.get(selectedProd), String.valueOf(qtdSpinner.getValue()));
                        //produtosUsados.add(puAdicionado);
                    }

                    //rowProdUsado[0] = puAdicionado.getProduto().getNome();
                    //rowProdUsado[1] = puAdicionado.getQuantidade();

                    //model2.addRow(rowProdUsado);

                    //atualizar listas
                    produtos.clear();
                    produtos.addAll(Produto.getListaProdutos());

                    produtosUsados.clear();
                    produtosUsados.addAll(ProdutoUsado.getListaProdsUsados(idConsulta));

                    DefaultTableModel modelupdatedProd = (DefaultTableModel) prodsTbl.getModel();
                    DefaultTableModel modelupdatedProdUsado = (DefaultTableModel) prodsUsadosTbl.getModel();

                    modelupdatedProd.setRowCount(0);
                    modelupdatedProdUsado.setRowCount(0);

                    for (Produto p : produtos) {
                        modelupdatedProd.addRow(new Object[]{p.getNome(), String.valueOf(p.getQuantidade())
                        });
                    }

                    for (ProdutoUsado pu : produtosUsados) {
                        modelupdatedProdUsado.addRow(new Object[]{pu.getProduto().getNome(), String.valueOf(pu.getQuantidade())
                        });
                    }

                } else {
                    System.out.println("erro ao adicionar");
                }

            }
        });
        removerBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedProdUsado = prodsUsadosTbl.getSelectedRow();
                //System.out.println(selected);

                if (selectedProdUsado >= 0) {

                    ProdutoUsado.deleteProdUsado(produtosUsados.get(selectedProdUsado).getProdutousadoPK());

                    model2.removeRow(selectedProdUsado);
                    produtosUsados.remove(selectedProdUsado);

                    //atualizar lista
                    produtos.clear();
                    produtos.addAll(Produto.getListaProdutos());

                    DefaultTableModel modelupdated = (DefaultTableModel) prodsTbl.getModel();

                    modelupdated.setRowCount(0);

                    for (Produto p : produtos) {
                        modelupdated.addRow(new Object[]{p.getNome(), String.valueOf(p.getQuantidade())
                        });
                    }

                } else {
                    System.out.println("erro ao remover");
                }
            }
        });
    }

    public void criarTabelas(int idConsulta) {

        String col[] = {"NOME", "QUANTIDADE"};

        //para que não dê para editar celulas
        TableModel modelT1 = new DefaultTableModel(null, col) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        TableModel modelT2 = new DefaultTableModel(null, col) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        prodsTbl.setModel(modelT1);
        prodsUsadosTbl.setModel(modelT2);

        TableColumnModel columns1 = prodsTbl.getColumnModel();
        TableColumnModel columns2 = prodsUsadosTbl.getColumnModel();

        /*Coloca os dados centrados na tabela*/
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment((int) JLabel.CENTER);

        columns1.getColumn(0).setCellRenderer(centerRenderer);
        columns1.getColumn(1).setCellRenderer(centerRenderer);

        columns2.getColumn(0).setCellRenderer(centerRenderer);
        columns2.getColumn(1).setCellRenderer(centerRenderer);

    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        prodsUsadosPnl = new JPanel();
        prodsUsadosPnl.setLayout(new GridLayoutManager(4, 5, new Insets(10, 10, 10, 10), -1, -1));
        puScroll = new JScrollPane();
        prodsUsadosPnl.add(puScroll, new GridConstraints(1, 3, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(300, 300), null, 0, false));
        prodsUsadosTbl = new JTable();
        puScroll.setViewportView(prodsUsadosTbl);
        qtdSpinner = new JSpinner();
        prodsUsadosPnl.add(qtdSpinner, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        pScroll = new JScrollPane();
        prodsUsadosPnl.add(pScroll, new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(300, 300), null, 0, false));
        prodsTbl = new JTable();
        pScroll.setViewportView(prodsTbl);
        final JLabel label1 = new JLabel();
        label1.setText("Quantidade:");
        prodsUsadosPnl.add(label1, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        adicionarBtn = new JButton();
        adicionarBtn.setText("Adicionar");
        prodsUsadosPnl.add(adicionarBtn, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        removerBtn = new JButton();
        removerBtn.setText("Remover");
        prodsUsadosPnl.add(removerBtn, new GridConstraints(3, 4, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("Produtos:");
        prodsUsadosPnl.add(label2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label3 = new JLabel();
        label3.setText("Produtos usados:");
        prodsUsadosPnl.add(label3, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        prodsUsadosPnl.add(spacer1, new GridConstraints(2, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return prodsUsadosPnl;
    }

}
