package UIForms;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import org.ipvcClinica.Consulta;
import org.ipvcClinica.Utente;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.format.DateTimeFormatter;


public class DadosConsulta extends JFrame {
    private JPanel DadosConsultaPanel;
    private JLabel ConsultaN;
    private JLabel sala;
    private JComboBox comboBoxSala;
    private JComboBox comboBoxEstado;
    private JLabel utenteLabel;
    private JLabel procLabel;
    private JLabel dentistaLabel;
    private JLabel dataLabel;
    private JLabel horaLabel;
    private JButton detalhesUtenteButton;
    private JButton receitasButton;
    private JButton produtosButton;
    private JButton confirmarButton;
    private JButton faturaButton;


    public DadosConsulta(int idConsulta, FuncionarioMain funcionarioMain) {

        super("Detalhes da Consulta");
        this.setContentPane(this.DadosConsultaPanel);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(1000, 700);
        this.setLocationRelativeTo(null);


        Consulta c = Consulta.getConsulta(idConsulta);

        DateTimeFormatter formatterHoras = DateTimeFormatter.ofPattern("HH:mm");
        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");


        /**
         * Coloca na frame os dados desta consulta.
         * */
        ConsultaN.setText("Consulta Nº" + idConsulta);
        utenteLabel.setText(c.getIdutente().getNome());
        procLabel.setText(c.getProcedimento().getNome());
        dentistaLabel.setText("Dr(a). " + c.getIddent().getNome());

        dataLabel.setText(formatterData.format(c.getDatainicio()));
        horaLabel.setText(formatterHoras.format(c.getDatainicio()) + "h - " + formatterHoras.format(c.getDatafim()) + "h");


        /**
         * Adiciona as opções às várias comboboxes(sala e estado da consulta).
         * */
        comboBoxEstado.addItem("Confirmada");
        comboBoxEstado.addItem("Paga");
        comboBoxEstado.addItem("Cancelada");

        comboBoxSala.addItem(1);
        comboBoxSala.addItem(2);
        comboBoxSala.addItem(3);
        comboBoxSala.addItem(4);
        comboBoxSala.addItem(5);
        comboBoxSala.addItem(6);


        /**
         * Obtém o input selecionado nas combobox relativo à sala e estado
         * da consulta(para efeitos de alteração de dados).
         * */
        comboBoxSala.setSelectedItem(c.getSala());
        comboBoxEstado.setSelectedItem(c.getEstado());


        /**
         * Ao clicar no botão Confirmar Alterações, os dados da consulta são atualizados mediante o selecionado
         * nas comboBoxes.
         * */
        confirmarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int sala = (int) comboBoxSala.getSelectedItem();

                String estado = (String) comboBoxEstado.getSelectedItem();

                Consulta.alteraEstado(idConsulta, sala, estado);

                JOptionPane.showMessageDialog(null, "Alterações realizadas com sucesso!");

                /**Volta a chamar os métodos para atualizar as tabelas.*/
                funcionarioMain.mostraConsultas();
                funcionarioMain.mostraConsultasConfirmadas();
                funcionarioMain.mostraConsultasHoje();
                funcionarioMain.mostraConsultasHistorico();
            }
        });


        /**
         * Ao clicar no botão Detalhes Utente mostra os detalhes do utente numa nova janela.
         * */
        detalhesUtenteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Utente utenteSel = Utente.getUtente(c.getIdutente().getIdutente());
                DadosUtente dadosU = new DadosUtente(utenteSel.getIdutente());
                dadosU.setVisible(true);
            }
        });

        /**
         * Ao clicar no botão Fatura mostra a fatua numa nova janela, caso ela exista.
         * Caso contrário,mostra um alerta mediante a situação.
         * */
        faturaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (c.getEstado().equals("Paga")) {
                    FaturaDados f = new FaturaDados(c.getIdconsulta());
                    f.setVisible(true);
                } else if (c.getEstado().equals("Confirmada")) {
                    JOptionPane.showMessageDialog(null, "Fatura indisponível! A consulta ainda não foi paga.");
                } else {
                    JOptionPane.showMessageDialog(null, "Fatura indisponível! A consulta foi cancelada.");
                }

            }
        });

        /**
         * Ao clicar no botão Receitas mostra as receitas associadas a determinada consulta numa nova janela.
         * */
        receitasButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                verReceitas receitas = new verReceitas(c.getIdconsulta());
                receitas.setVisible(true);
            }
        });

        /**
         * Ao clicar no botão Produtos mostra os produtos utilizados nessa consulta, numa nova janela.
         * */
        produtosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                verProdutos produtos = new verProdutos(c.getIdconsulta());
                produtos.setVisible(true);
            }
        });
    }


    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        DadosConsultaPanel = new JPanel();
        DadosConsultaPanel.setLayout(new GridLayoutManager(12, 7, new Insets(10, 40, 10, 10), -1, -1));
        final Spacer spacer1 = new Spacer();
        DadosConsultaPanel.add(spacer1, new GridConstraints(0, 6, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        ConsultaN = new JLabel();
        Font ConsultaNFont = this.$$$getFont$$$("Arial Black", Font.PLAIN, 18, ConsultaN.getFont());
        if (ConsultaNFont != null) ConsultaN.setFont(ConsultaNFont);
        ConsultaN.setText("ConsultaLabel");
        DadosConsultaPanel.add(ConsultaN, new GridConstraints(0, 0, 1, 6, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label1 = new JLabel();
        Font label1Font = this.$$$getFont$$$(null, Font.BOLD, -1, label1.getFont());
        if (label1Font != null) label1.setFont(label1Font);
        label1.setText("Utente:");
        DadosConsultaPanel.add(label1, new GridConstraints(2, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        procLabel = new JLabel();
        procLabel.setText("Label");
        DadosConsultaPanel.add(procLabel, new GridConstraints(3, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        dentistaLabel = new JLabel();
        dentistaLabel.setText("Label");
        DadosConsultaPanel.add(dentistaLabel, new GridConstraints(4, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        dataLabel = new JLabel();
        dataLabel.setText("Label");
        DadosConsultaPanel.add(dataLabel, new GridConstraints(5, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        Font label2Font = this.$$$getFont$$$(null, Font.BOLD, -1, label2.getFont());
        if (label2Font != null) label2.setFont(label2Font);
        label2.setText("Procedimento:");
        DadosConsultaPanel.add(label2, new GridConstraints(3, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label3 = new JLabel();
        Font label3Font = this.$$$getFont$$$(null, Font.BOLD, -1, label3.getFont());
        if (label3Font != null) label3.setFont(label3Font);
        label3.setText("Dentista:");
        DadosConsultaPanel.add(label3, new GridConstraints(4, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        utenteLabel = new JLabel();
        utenteLabel.setText("Label");
        DadosConsultaPanel.add(utenteLabel, new GridConstraints(2, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        horaLabel = new JLabel();
        horaLabel.setText("Label");
        DadosConsultaPanel.add(horaLabel, new GridConstraints(6, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label4 = new JLabel();
        Font label4Font = this.$$$getFont$$$(null, Font.BOLD, -1, label4.getFont());
        if (label4Font != null) label4.setFont(label4Font);
        label4.setText("Data: ");
        DadosConsultaPanel.add(label4, new GridConstraints(5, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label5 = new JLabel();
        Font label5Font = this.$$$getFont$$$(null, Font.BOLD, -1, label5.getFont());
        if (label5Font != null) label5.setFont(label5Font);
        label5.setText("Hora Início/Fim:");
        DadosConsultaPanel.add(label5, new GridConstraints(6, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        sala = new JLabel();
        Font salaFont = this.$$$getFont$$$(null, Font.BOLD, -1, sala.getFont());
        if (salaFont != null) sala.setFont(salaFont);
        sala.setText("Sala: ");
        DadosConsultaPanel.add(sala, new GridConstraints(7, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        comboBoxSala = new JComboBox();
        DadosConsultaPanel.add(comboBoxSala, new GridConstraints(7, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label6 = new JLabel();
        Font label6Font = this.$$$getFont$$$(null, Font.BOLD, -1, label6.getFont());
        if (label6Font != null) label6.setFont(label6Font);
        label6.setText("Estado: ");
        DadosConsultaPanel.add(label6, new GridConstraints(8, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        comboBoxEstado = new JComboBox();
        DadosConsultaPanel.add(comboBoxEstado, new GridConstraints(8, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer2 = new Spacer();
        DadosConsultaPanel.add(spacer2, new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        detalhesUtenteButton = new JButton();
        detalhesUtenteButton.setText("Detalhes Utente");
        DadosConsultaPanel.add(detalhesUtenteButton, new GridConstraints(10, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        receitasButton = new JButton();
        receitasButton.setText("Receitas");
        DadosConsultaPanel.add(receitasButton, new GridConstraints(10, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer3 = new Spacer();
        DadosConsultaPanel.add(spacer3, new GridConstraints(9, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        produtosButton = new JButton();
        produtosButton.setText("Produtos");
        DadosConsultaPanel.add(produtosButton, new GridConstraints(11, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        faturaButton = new JButton();
        faturaButton.setText("Fatura");
        DadosConsultaPanel.add(faturaButton, new GridConstraints(11, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        confirmarButton = new JButton();
        confirmarButton.setText("Confirmar Alterações");
        DadosConsultaPanel.add(confirmarButton, new GridConstraints(10, 4, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return DadosConsultaPanel;
    }

}
