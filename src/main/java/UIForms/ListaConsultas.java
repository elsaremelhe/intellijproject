package UIForms;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import org.ipvcClinica.*;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.time.format.DateTimeFormatter;
import javax.swing.table.DefaultTableModel;
import java.util.List;

public class ListaConsultas extends JFrame {

    private JPanel listaConPanel;
    private JTable table1;
    private JLabel titulo;
    private javax.swing.JScrollPane JScrollPane;
    private JComboBox comboBox;
    private JTabbedPane tabbedPane1;
    private JPanel Consultas;
    private JPanel JPanel2;
    private JPanel Utentes;
    private JScrollPane JScrollPane2;
    private JTable utentesTable;
    private JTextField searchTextField;
    private JLabel UtentesLabel;
    private JPanel Produtos;
    private JTable produtosTable;
    private JTextField searchFieldProd;
    private JPanel Dentistas;
    private JTable dentistasTable;
    private JTextField searchFieldDent;
    private JPanel Procedimentos;
    private JTable procedimentosTable;
    private JTextField searchProced;
    private JLabel infoDent;
    private JButton logoutButton;


    public ListaConsultas(int params) {

        super("Menu Dentista");
        this.setContentPane(this.listaConPanel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(1400, 800);
        this.setLocationRelativeTo(null);


        tabbedPane1.addTab("Consultas", Consultas);
        tabbedPane1.addTab("Utentes", Utentes);
        tabbedPane1.addTab("Produtos", Produtos);
        tabbedPane1.addTab("Procedimentos", Procedimentos);


        /*Obtém a informação da conta em que se está autenticado.*/
        Dentista d = Dentista.getDentistaPorConta(params);

        infoDent.setText("Dr(a): " + d.getNome());


        /**Filtros para consultas(Menu Consultas)*/
        comboBox.addItem("Todas");
        comboBox.addItem("Hoje");
        comboBox.addItem("Confirmadas");
        comboBox.addItem("Histórico");

        comboBox.setSelectedItem("Hoje");
        createTable();
        mostraConsultasHoje(params);


        comboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String selected = (String) comboBox.getSelectedItem();

                if (selected.equals("Todas")) {
                    System.out.println("Mostra todas as consultas");
                    mostraConsultas(params);
                } else if (selected.equals("Hoje")) {
                    System.out.println("Só mostra as consultas de hoje");
                    mostraConsultasHoje(params);
                } else if (selected.equals("Confirmadas")) {
                    mostraConsultasConfirmadas(params);
                    System.out.println("Só mostra as consultas confirmadas");
                } else if (selected.equals("Histórico")) {
                    mostraConsultasHistorico(params);
                    System.out.println("Mostra o histórico das consultas");
                }
            }
        });

        table1.addComponentListener(new ComponentAdapter() {
        });
        table1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);

                int index = table1.getSelectedRow();
                TableModel model = table1.getModel();
                int id = Integer.parseInt(model.getValueAt(index, 0).toString());
                /*String estado = model.getValueAt(index, 1).toString();
                String data = model.getValueAt(index, 2).toString();
                String horaIni = model.getValueAt(index,3).toString();
                String horaFim = model.getValueAt(index, 4).toString();
                String sala = model.getValueAt(index,5).toString();*/


                ConsultaDetalhes cd = new ConsultaDetalhes(id);
                cd.setVisible(true);
            }
        });

        /**Tab Utentes*/
        createTableUtentes();
        mostraUtentes();

        /*Search Bar para a Lista de utentes*/
        searchTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {

                TableModel model = utentesTable.getModel();
                String search = searchTextField.getText();

                TableRowSorter<TableModel> tr = new TableRowSorter<>(model);

                utentesTable.setRowSorter(tr);
                tr.setRowFilter(RowFilter.regexFilter("(?i)" + search));

                super.keyReleased(e);
            }
        });


        utentesTable.addComponentListener(new ComponentAdapter() {
        });
        utentesTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);

                int index = utentesTable.getSelectedRow();
                TableModel model = utentesTable.getModel();
                int idUtente = Integer.parseInt(model.getValueAt(index, 0).toString());

                Utente u = Utente.getUtente(idUtente);

                UtenteDetalhes ud = new UtenteDetalhes(u);
                ud.setVisible(true);
            }
        });

        /*Tab Produtos*/
        createTableProdutos();
        mostraProdutos();

        /*Search Bar para a lista de produtos*/
        searchFieldProd.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {

                TableModel model = produtosTable.getModel();
                String search = searchFieldProd.getText();

                TableRowSorter<TableModel> tr = new TableRowSorter<>(model);

                produtosTable.setRowSorter(tr);
                tr.setRowFilter(RowFilter.regexFilter("(?i)" + search));

                super.keyReleased(e);
            }
        });



        /*Tab Procedimentos*/
        createTableProcedimentos();
        mostraProcedimentos();

        /*Search Bar para a lista de procedimentos*/
        searchProced.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {

                TableModel model = procedimentosTable.getModel();
                String search = searchProced.getText();

                TableRowSorter<TableModel> tr = new TableRowSorter<>(model);

                procedimentosTable.setRowSorter(tr);
                tr.setRowFilter(RowFilter.regexFilter("(?i)" + search));

                super.keyReleased(e);
            }
        });

        logoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int option = JOptionPane.showConfirmDialog(
                        listaConPanel,
                        "Tem a certeza que pretende sair?",
                        "Logout",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (option == JOptionPane.YES_OPTION) {
                    Window win[] = Window.getWindows();
                    for (int i = 1; i < win.length; i++) {
                        win[i].dispose();
                        win[i] = null;
                    }
                }


            }
        });

    }

    /**
     * Insere na tabela a lista das consultas provenientes da BD.
     */
    private void mostraConsultas(int params) {

        List<Consulta> consultas = Consulta.getConsultasPorDentista(params);
        DefaultTableModel model = (DefaultTableModel) table1.getModel();

        DateTimeFormatter formatterHoras = DateTimeFormatter.ofPattern("HH:mm");
        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        model.setRowCount(0);


        for (Consulta c : consultas) {

            model.addRow(new Object[]{String.valueOf(c.getIdconsulta()), c.getEstado(), formatterData.format(c.getDatainicio()), formatterHoras.format(c.getDatainicio()) + "h"});
        }
    }

    /**
     * Cria a tabela para mostrar a lista das consultas.
     */
    public void createTable() {

        String col[] = {"ID", "ESTADO", "DATA", "HORA INÍCIO"};

        //para que não dê para editar celulas
        TableModel model = new DefaultTableModel(null, col) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        //table1.setModel(new DefaultTableModel(null, col));
        table1.setModel(model);

        TableColumnModel columns = table1.getColumnModel();

        columns.getColumn(0).setPreferredWidth(10);

        /*Coloca os dados centrados na tabela*/
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment((int) JLabel.CENTER);

        columns.getColumn(0).setCellRenderer(centerRenderer);
        columns.getColumn(1).setCellRenderer(centerRenderer);
        columns.getColumn(2).setCellRenderer(centerRenderer);


    }

    /**
     * Cria a tabela para mostrar a lista dos utentes.
     */
    private void createTableUtentes() {

        String col[] = {"ID", "NOME", "CONTACTO"};

        //para que não dê para editar celulas
        TableModel model = new DefaultTableModel(null, col) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        utentesTable.setModel(model);

        TableColumnModel columns = utentesTable.getColumnModel();


        columns.getColumn(0).setPreferredWidth(10);


        //Coloca os dados centrados na tabela
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment((int) JLabel.CENTER);

        columns.getColumn(0).setCellRenderer(centerRenderer);
        columns.getColumn(1).setCellRenderer(centerRenderer);
        columns.getColumn(2).setCellRenderer(centerRenderer);


    }

    /**
     * Cria a tabela para mostrar a lista dos produtos.
     */
    private void createTableProdutos() {

        String col[] = {"ID", "NOME", "QUANTIDADE"};

        //para que não dê para editar celulas
        TableModel model = new DefaultTableModel(null, col) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        produtosTable.setModel(model);

        TableColumnModel columns = produtosTable.getColumnModel();

        columns.getColumn(0).setPreferredWidth(10);

        //Coloca os dados centrados na tabela
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment((int) JLabel.CENTER);

        columns.getColumn(0).setCellRenderer(centerRenderer);
        columns.getColumn(1).setCellRenderer(centerRenderer);
        columns.getColumn(2).setCellRenderer(centerRenderer);

    }


    /**
     * Cria a tabela para mostrar a lista dos procedimentos.
     */
    private void createTableProcedimentos() {

        String col[] = {"ID", "NOME", "PREÇO", "DURAÇÃO"};

        //para que não dê para editar celulas
        TableModel model = new DefaultTableModel(null, col) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        procedimentosTable.setModel(model);

        TableColumnModel columns = procedimentosTable.getColumnModel();

        columns.getColumn(0).setPreferredWidth(10);

        //Coloca os dados centrados na tabela
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment((int) JLabel.CENTER);

        columns.getColumn(0).setCellRenderer(centerRenderer);
        columns.getColumn(1).setCellRenderer(centerRenderer);
        columns.getColumn(2).setCellRenderer(centerRenderer);
        columns.getColumn(3).setCellRenderer(centerRenderer);


    }


    /**
     * Insere na tabela todas as consultas como estado seja "Confirmada".
     */
    private void mostraConsultasConfirmadas(int params) {

        List<Consulta> consultas = Consulta.getConsultasPorDentistaConfirmadas(params);
        DefaultTableModel model = (DefaultTableModel) table1.getModel();


        DateTimeFormatter formatterHoras = DateTimeFormatter.ofPattern("HH:mm");
        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        model.setRowCount(0);

        for (Consulta c : consultas) {

            model.addRow(new Object[]{String.valueOf(c.getIdconsulta()), c.getEstado(), formatterData.format(c.getDatainicio()), formatterHoras.format(c.getDatainicio()) + "h"});
        }
    }

    /**
     * Insere na tabela as consultas agendadas para o dia atual de determinado dentista.
     */
    private void mostraConsultasHoje(int params) {

        List<Consulta> consultas = Consulta.getConsultasPorDentistaHoje(params);
        DefaultTableModel model = (DefaultTableModel) table1.getModel();


        DateTimeFormatter formatterHoras = DateTimeFormatter.ofPattern("HH:mm");
        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        model.setRowCount(0);

        for (Consulta c : consultas) {

            model.addRow(new Object[]{String.valueOf(c.getIdconsulta()), c.getEstado(), formatterData.format(c.getDatainicio()), formatterHoras.format(c.getDatainicio()) + "h"});
        }
    }

    /**
     * Insere na tabela as consultas anteriores ao dia atual, ou seja, o histórico de consultas.
     */
    private void mostraConsultasHistorico(int params) {

        List<Consulta> consultas = Consulta.getConsultasPorDentistaHistorico(params);
        DefaultTableModel model = (DefaultTableModel) table1.getModel();


        DateTimeFormatter formatterHoras = DateTimeFormatter.ofPattern("HH:mm");
        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        model.setRowCount(0);

        for (Consulta c : consultas) {
            //formatterFim.format(c.getDatafim())
            model.addRow(new Object[]{String.valueOf(c.getIdconsulta()), c.getEstado(), formatterData.format(c.getDatainicio()), formatterHoras.format(c.getDatainicio()) + "h"});
        }
    }


    /**
     * Insere na tabela a lista dos utentes provenientes da BD.
     */
    private void mostraUtentes() {


        List<Utente> utentes = Utente.getListaUtentes();
        DefaultTableModel model = (DefaultTableModel) utentesTable.getModel();


        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        model.setRowCount(0);

        for (Utente u : utentes) {
            model.addRow(new Object[]{String.valueOf(u.getIdutente()), u.getNome(), u.getContacto()});
        }
    }

    /**
     * Insere na tabela a lista dos produtos provenientes da BD.
     */
    private void mostraProdutos() {

        List<Produto> produtos = Produto.getListaProdutos();
        DefaultTableModel model = (DefaultTableModel) produtosTable.getModel();

        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        model.setRowCount(0);

        for (Produto p : produtos) {
            model.addRow(new Object[]{String.valueOf(p.getIdprod()), p.getNome(), String.valueOf(p.getQuantidade())});
        }
    }


    /**
     * Insere na tabela a lista dos procedimentos provenientes da BD.
     */
    private void mostraProcedimentos() {

        List<Procedimento> proceds = Procedimento.getListaProcedimentos();
        DefaultTableModel model = (DefaultTableModel) procedimentosTable.getModel();


        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        model.setRowCount(0);

        for (Procedimento p : proceds) {
            model.addRow(new Object[]{String.valueOf(p.getIdproced()), p.getNome(), p.getPreco() + "€", p.getDuracao() + "h"});
        }
    }


    private void createUIComponents() {

    }


    public static void main(String[] args) {

        //ListaConsultas listaConsultas = new ListaConsultas();
        //listaConsultas.setVisible(true);

    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        listaConPanel = new JPanel();
        listaConPanel.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane1 = new JTabbedPane();
        tabbedPane1.setToolTipText("");
        listaConPanel.add(tabbedPane1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(200, 200), null, 0, false));
        Consultas = new JPanel();
        Consultas.setLayout(new GridLayoutManager(3, 5, new Insets(0, 0, 0, 0), -1, -1));
        Consultas.setToolTipText("");
        tabbedPane1.addTab("Untitled", Consultas);
        JScrollPane = new JScrollPane();
        Consultas.add(JScrollPane, new GridConstraints(2, 0, 1, 5, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        table1 = new JTable();
        JScrollPane.setViewportView(table1);
        JPanel2 = new JPanel();
        JPanel2.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        Consultas.add(JPanel2, new GridConstraints(0, 1, 1, 4, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        titulo = new JLabel();
        Font tituloFont = this.$$$getFont$$$("Ayuthaya", Font.BOLD, 18, titulo.getFont());
        if (tituloFont != null) titulo.setFont(tituloFont);
        titulo.setText("Consultas");
        JPanel2.add(titulo, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        comboBox = new JComboBox();
        Consultas.add(comboBox, new GridConstraints(1, 4, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        Consultas.add(panel1, new GridConstraints(1, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        Utentes = new JPanel();
        Utentes.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane1.addTab("Untitled", Utentes);
        JScrollPane2 = new JScrollPane();
        Utentes.add(JScrollPane2, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        utentesTable = new JTable();
        JScrollPane2.setViewportView(utentesTable);
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        Utentes.add(panel2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        searchTextField = new JTextField();
        panel2.add(searchTextField, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel2.add(panel3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label1 = new JLabel();
        Font label1Font = this.$$$getFont$$$("Arial", Font.PLAIN, 14, label1.getFont());
        if (label1Font != null) label1.setFont(label1Font);
        label1.setText("Procurar:");
        panel3.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel4 = new JPanel();
        panel4.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        Utentes.add(panel4, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        UtentesLabel = new JLabel();
        Font UtentesLabelFont = this.$$$getFont$$$("Ayuthaya", Font.BOLD, 18, UtentesLabel.getFont());
        if (UtentesLabelFont != null) UtentesLabel.setFont(UtentesLabelFont);
        UtentesLabel.setHorizontalAlignment(11);
        UtentesLabel.setText("Utentes");
        panel4.add(UtentesLabel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        Produtos = new JPanel();
        Produtos.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane1.addTab("Untitled", Produtos);
        final JPanel panel5 = new JPanel();
        panel5.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        Produtos.add(panel5, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        Font label2Font = this.$$$getFont$$$("Ayuthaya", Font.BOLD, 18, label2.getFont());
        if (label2Font != null) label2.setFont(label2Font);
        label2.setText("Produtos");
        panel5.add(label2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final javax.swing.JScrollPane scrollPane1 = new JScrollPane();
        Produtos.add(scrollPane1, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        produtosTable = new JTable();
        scrollPane1.setViewportView(produtosTable);
        final JPanel panel6 = new JPanel();
        panel6.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        Produtos.add(panel6, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel7 = new JPanel();
        panel7.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel6.add(panel7, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label3 = new JLabel();
        Font label3Font = this.$$$getFont$$$("Arial", -1, 14, label3.getFont());
        if (label3Font != null) label3.setFont(label3Font);
        label3.setText("Procurar:");
        panel7.add(label3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel8 = new JPanel();
        panel8.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel6.add(panel8, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        searchFieldProd = new JTextField();
        panel8.add(searchFieldProd, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        Procedimentos = new JPanel();
        Procedimentos.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane1.addTab("Untitled", Procedimentos);
        final JPanel panel9 = new JPanel();
        panel9.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        Procedimentos.add(panel9, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label4 = new JLabel();
        Font label4Font = this.$$$getFont$$$("Ayuthaya", Font.BOLD, 18, label4.getFont());
        if (label4Font != null) label4.setFont(label4Font);
        label4.setText("Procedimentos");
        panel9.add(label4, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final javax.swing.JScrollPane scrollPane2 = new JScrollPane();
        Procedimentos.add(scrollPane2, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        procedimentosTable = new JTable();
        scrollPane2.setViewportView(procedimentosTable);
        final JPanel panel10 = new JPanel();
        panel10.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        Procedimentos.add(panel10, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel11 = new JPanel();
        panel11.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel10.add(panel11, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label5 = new JLabel();
        Font label5Font = this.$$$getFont$$$("Arial", -1, 14, label5.getFont());
        if (label5Font != null) label5.setFont(label5Font);
        label5.setText("Procurar:");
        panel11.add(label5, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel12 = new JPanel();
        panel12.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel10.add(panel12, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        searchProced = new JTextField();
        panel12.add(searchProced, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        infoDent = new JLabel();
        Font infoDentFont = this.$$$getFont$$$("Arial", -1, 18, infoDent.getFont());
        if (infoDentFont != null) infoDent.setFont(infoDentFont);
        infoDent.setText("infoDent");
        listaConPanel.add(infoDent, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        logoutButton = new JButton();
        logoutButton.setText("Logout");
        listaConPanel.add(logoutButton, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return listaConPanel;
    }

}
