package UIForms;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import org.ipvcClinica.ProdutoUsado;


import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.util.List;

public class verProdutos extends JFrame {
    private JPanel verProdutosPanel;
    private JLabel ProdutosLabel;
    private JTable tableProdutosUti;

    public verProdutos(int idConsulta) {

        super("Produtos Utilizados");

        this.setContentPane(this.verProdutosPanel);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(600, 400);
        this.setLocationRelativeTo(null);


        criaTabela();
        mostraProdutosUsados(idConsulta);


    }

    /**
     * Cria a tabela para mostrar a lista dos produtos usados.
     */
    private void criaTabela() {

        String col[] = {"PRODUTO", "QUANTIDADE"};

        //para que não dê para editar celulas
        TableModel model = new DefaultTableModel(null, col) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tableProdutosUti.setModel(model);

        TableColumnModel columns = tableProdutosUti.getColumnModel();


        //Coloca os dados centrados na tabela
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment((int) JLabel.CENTER);

        columns.getColumn(0).setCellRenderer(centerRenderer);
        columns.getColumn(1).setCellRenderer(centerRenderer);


    }

    /**
     * Insere na tabela as receitas de determinada consulta.
     *
     * @param idConsulta
     */
    private void mostraProdutosUsados(int idConsulta) {

        List<ProdutoUsado> produtos = ProdutoUsado.getListaProdsUsados(idConsulta);
        DefaultTableModel model = (DefaultTableModel) tableProdutosUti.getModel();


        model.setRowCount(0);

        for (ProdutoUsado p : produtos) {

            model.addRow(new Object[]{p.getProduto().getNome(), p.getQuantidade()
            });
        }
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        verProdutosPanel = new JPanel();
        verProdutosPanel.setLayout(new GridLayoutManager(2, 1, new Insets(10, 10, 10, 10), -1, -1));
        ProdutosLabel = new JLabel();
        Font ProdutosLabelFont = this.$$$getFont$$$("Arial Black", Font.PLAIN, 16, ProdutosLabel.getFont());
        if (ProdutosLabelFont != null) ProdutosLabel.setFont(ProdutosLabelFont);
        ProdutosLabel.setText("Produtos Utilizados");
        verProdutosPanel.add(ProdutosLabel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JScrollPane scrollPane1 = new JScrollPane();
        verProdutosPanel.add(scrollPane1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        tableProdutosUti = new JTable();
        scrollPane1.setViewportView(tableProdutosUti);
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return verProdutosPanel;
    }

}
