package UIForms;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import org.ipvcClinica.Consulta;
import org.ipvcClinica.Utente;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.*;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class DadosUtente extends JFrame {
    private JPanel DetalhesUtenteTab;
    private JLabel nomeLabel;
    private JLabel dataNascLabel;
    private JLabel moradaLabel;
    private JLabel nifLabel;
    private JLabel nCCLabel;
    private JLabel utenteIDLabel;
    private JButton confirmarButton;
    private JTextField moradaTextfield;
    private JTextField contactoTextfield;
    private JTabbedPane tabbedPane;
    private JPanel HistoricoTab;
    private JPanel detalhesPanel;
    private JTable historicoConTable;
    private JPasswordField nifTextfield;


    public DadosUtente(int idUtente) {

        super("Detalhes do Utente");
        this.setContentPane(this.detalhesPanel);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(900, 650);
        this.setLocationRelativeTo(null);


        tabbedPane.addTab("Detalhes do Utente", DetalhesUtenteTab);
        tabbedPane.addTab("Histórico de Consultas", HistoricoTab);


        Utente ut = Utente.getUtente(idUtente);

        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");


        /**
         * Coloca na frame os dados do utente associado a esta consulta.
         * */
        utenteIDLabel.setText("Nº de Utente: " + idUtente);
        nomeLabel.setText(ut.getNome());
        dataNascLabel.setText(formatterData.format(ut.getDatanascimento()));
        contactoTextfield.setText(ut.getContacto());
        moradaTextfield.setText(ut.getMorada());
        nifLabel.setText(ut.getNif());
        nCCLabel.setText(ut.getNumcc());


        /**
         * Ao clicar no botão Confirmar Alterações, os dados do utente são atualizados mediante o escrito nos TextFields
         *
         * */
        confirmarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (moradaTextfield.toString() != null && contactoTextfield.toString() != null) {
                    Utente.alteraDados(idUtente, moradaTextfield.getText(), contactoTextfield.getText());

                    JOptionPane.showMessageDialog(null, "Alterações realizadas com sucesso!");
                } else {
                    JOptionPane.showMessageDialog(null, "Nenhum campo pode estar vazio!");
                }


            }
        });


        criarTabelaHistorico();
        mostraConsultasH(idUtente);


        /*Abre os detalhes da consulta.*/
        historicoConTable.addComponentListener(new ComponentAdapter() {
        });
        historicoConTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);

                int index = historicoConTable.getSelectedRow();
                TableModel model = historicoConTable.getModel();
                int idConsulta = Integer.parseInt(model.getValueAt(index, 0).toString());


                DadosConsulta c = new DadosConsulta(idConsulta, null);
                c.setSize(500, 500);
                c.setLocationRelativeTo(null);
                c.setVisible(true);

            }
        });


    }


    /**
     * Cria a tabela para mostrar a lista das consultas de determinado utente.
     */
    public void criarTabelaHistorico() {

        String col[] = {"ID", "ESTADO", "DATA", "HORA INÍCIO"};

        //para que não dê para editar celulas
        TableModel model = new DefaultTableModel(null, col) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };


        historicoConTable.setModel(model);

        TableColumnModel columns = historicoConTable.getColumnModel();

        columns.getColumn(0).setPreferredWidth(10);


        /*Coloca os dados centrados na tabela*/
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment((int) JLabel.CENTER);

        columns.getColumn(0).setCellRenderer(centerRenderer);
        columns.getColumn(1).setCellRenderer(centerRenderer);
        columns.getColumn(2).setCellRenderer(centerRenderer);
        columns.getColumn(3).setCellRenderer(centerRenderer);

    }

    /**
     * Insere na tabela a lista das consultas provenientes da BD.
     */
    private void mostraConsultasH(int idUtente) {


        List<Consulta> consultas = Consulta.getConsultasPorUtente(idUtente);
        DefaultTableModel model = (DefaultTableModel) historicoConTable.getModel();

        DateTimeFormatter formatterHoras = DateTimeFormatter.ofPattern("HH:mm");
        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        model.setRowCount(0);


        for (Consulta c : consultas) {

            model.addRow(new Object[]{String.valueOf(c.getIdconsulta()), c.getEstado(), formatterData.format(c.getDatainicio()), formatterHoras.format(c.getDatainicio()) + "h"});
        }
    }


    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        detalhesPanel = new JPanel();
        detalhesPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        detalhesPanel.setEnabled(false);
        tabbedPane = new JTabbedPane();
        detalhesPanel.add(tabbedPane, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        DetalhesUtenteTab = new JPanel();
        DetalhesUtenteTab.setLayout(new GridLayoutManager(17, 5, new Insets(10, 30, 10, 10), -1, -1));
        tabbedPane.addTab("Untitled", DetalhesUtenteTab);
        utenteIDLabel = new JLabel();
        Font utenteIDLabelFont = this.$$$getFont$$$("Arial Black", -1, 18, utenteIDLabel.getFont());
        if (utenteIDLabelFont != null) utenteIDLabel.setFont(utenteIDLabelFont);
        utenteIDLabel.setText("Utente id");
        DetalhesUtenteTab.add(utenteIDLabel, new GridConstraints(0, 0, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        DetalhesUtenteTab.add(spacer1, new GridConstraints(0, 4, 13, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final JLabel label1 = new JLabel();
        Font label1Font = this.$$$getFont$$$(null, Font.BOLD, -1, label1.getFont());
        if (label1Font != null) label1.setFont(label1Font);
        label1.setText("Nome:");
        DetalhesUtenteTab.add(label1, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        Font label2Font = this.$$$getFont$$$(null, Font.BOLD, -1, label2.getFont());
        if (label2Font != null) label2.setFont(label2Font);
        label2.setText("Data de Nascimento:");
        DetalhesUtenteTab.add(label2, new GridConstraints(4, 0, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label3 = new JLabel();
        Font label3Font = this.$$$getFont$$$(null, Font.BOLD, -1, label3.getFont());
        if (label3Font != null) label3.setFont(label3Font);
        label3.setText("Contacto:");
        DetalhesUtenteTab.add(label3, new GridConstraints(6, 0, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label4 = new JLabel();
        Font label4Font = this.$$$getFont$$$(null, Font.BOLD, -1, label4.getFont());
        if (label4Font != null) label4.setFont(label4Font);
        label4.setText("Morada");
        DetalhesUtenteTab.add(label4, new GridConstraints(8, 0, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label5 = new JLabel();
        Font label5Font = this.$$$getFont$$$(null, Font.BOLD, -1, label5.getFont());
        if (label5Font != null) label5.setFont(label5Font);
        label5.setText("NIF: ");
        DetalhesUtenteTab.add(label5, new GridConstraints(10, 0, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label6 = new JLabel();
        Font label6Font = this.$$$getFont$$$(null, Font.BOLD, -1, label6.getFont());
        if (label6Font != null) label6.setFont(label6Font);
        label6.setText("Nº Cartão de Cidadão:");
        DetalhesUtenteTab.add(label6, new GridConstraints(12, 0, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        dataNascLabel = new JLabel();
        dataNascLabel.setText("dataNasc");
        DetalhesUtenteTab.add(dataNascLabel, new GridConstraints(4, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        nCCLabel = new JLabel();
        nCCLabel.setText("numCC");
        DetalhesUtenteTab.add(nCCLabel, new GridConstraints(12, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer2 = new Spacer();
        DetalhesUtenteTab.add(spacer2, new GridConstraints(14, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final Spacer spacer3 = new Spacer();
        DetalhesUtenteTab.add(spacer3, new GridConstraints(13, 0, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final Spacer spacer4 = new Spacer();
        DetalhesUtenteTab.add(spacer4, new GridConstraints(11, 0, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final Spacer spacer5 = new Spacer();
        DetalhesUtenteTab.add(spacer5, new GridConstraints(9, 0, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final Spacer spacer6 = new Spacer();
        DetalhesUtenteTab.add(spacer6, new GridConstraints(7, 0, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final Spacer spacer7 = new Spacer();
        DetalhesUtenteTab.add(spacer7, new GridConstraints(5, 0, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final Spacer spacer8 = new Spacer();
        DetalhesUtenteTab.add(spacer8, new GridConstraints(3, 0, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final Spacer spacer9 = new Spacer();
        DetalhesUtenteTab.add(spacer9, new GridConstraints(1, 0, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        nomeLabel = new JLabel();
        nomeLabel.setText("nomeU");
        DetalhesUtenteTab.add(nomeLabel, new GridConstraints(2, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer10 = new Spacer();
        DetalhesUtenteTab.add(spacer10, new GridConstraints(15, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        moradaTextfield = new JTextField();
        DetalhesUtenteTab.add(moradaTextfield, new GridConstraints(8, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        contactoTextfield = new JTextField();
        contactoTextfield.setText("");
        DetalhesUtenteTab.add(contactoTextfield, new GridConstraints(6, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        nifLabel = new JLabel();
        nifLabel.setText("Label");
        DetalhesUtenteTab.add(nifLabel, new GridConstraints(10, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        confirmarButton = new JButton();
        confirmarButton.setText("Confirmar Alterações");
        DetalhesUtenteTab.add(confirmarButton, new GridConstraints(16, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        HistoricoTab = new JPanel();
        HistoricoTab.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane.addTab("Untitled", HistoricoTab);
        final JScrollPane scrollPane1 = new JScrollPane();
        HistoricoTab.add(scrollPane1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        historicoConTable = new JTable();
        scrollPane1.setViewportView(historicoConTable);
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return detalhesPanel;
    }

}
