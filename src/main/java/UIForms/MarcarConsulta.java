package UIForms;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import org.ipvcClinica.*;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class MarcarConsulta extends JFrame implements PropertyChangeListener {
    private JComboBox utenteSelect;
    private JComboBox horaTxt;
    private JComboBox procedSelect;
    private JComboBox dentistaSelect;
    private JButton marcarConsultaBtn;
    private JPanel MarcarConsultaPnl;
    private JFormattedTextField datePickerTxt;
    private JButton button1;
    private LocalDateTime currentDate;

    public MarcarConsulta() {

        super("Marcar Nova Consulta");

        this.setContentPane(this.MarcarConsultaPnl);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(550, 400);
        this.setLocationRelativeTo(null);

        List<Utente> utentes = Utente.getListaUtentes();
        List<Dentista> dentistas = Dentista.getListaDentistas();
        List<Procedimento> proceds = Procedimento.getListaProcedimentos();

        populateComboBoxes(utentes, dentistas, proceds);

        DateTimeFormatter formatterHoras = DateTimeFormatter.ofPattern("HH:mm");
        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        currentDate = LocalDateTime.now();

        //datePickerTxt.setValue(new Date());
        datePickerTxt.setValue(formatterData.format(currentDate));

        Calendario cal = new Calendario();
        cal.setUndecorated(true);
        cal.addPropertyChangeListener(this);

        /*
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });*/
        marcarConsultaBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!datePickerTxt.getValue().equals("")) {

                    System.out.println(dentistas.get(dentistaSelect.getSelectedIndex()).getIddent());
                    //List<Consulta> consultas = Consulta.getConsultasPorDentista(dentistas.get(dentistaSelect.getSelectedIndex()).getConta().getIdconta());
                    List<Consulta> consultas = Consulta.getListaConsultas();
                    int fimEstimado = proceds.get(procedSelect.getSelectedIndex()).getDuracao();

                    String str = datePickerTxt.getText() + " " + horaTxt.getSelectedItem();
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

                    LocalDateTime dataHoraInicio = LocalDateTime.parse(str, formatter);
                    LocalDateTime dataHoraFim = dataHoraInicio.plusHours(fimEstimado);

                    Boolean hourOccupied = false;
                    List<Integer> salasOcupadas = new ArrayList<>();

                    for (Consulta c : consultas) {

                        //switch(true) não dá em java, não sei como pôr esta porção melhor ;-;
                        if (dataHoraFim.isBefore(c.getDatafim()) && dataHoraFim.isAfter(c.getDatainicio())) {
                            if (c.getIddent().getNome().equals(dentistaSelect.getSelectedItem())) {
                                hourOccupied = true;
                                JOptionPane.showMessageDialog(null, "Erro: Combinação Data/Hora/Doutor(a) indisponível");
                                break;
                            } else {
                                salasOcupadas.add(c.getSala());
                            }
                        } else if (dataHoraInicio.isAfter(c.getDatainicio()) && dataHoraInicio.isBefore(c.getDatafim())) {
                            if (c.getIddent().getNome().equals(dentistaSelect.getSelectedItem())) {
                                hourOccupied = true;
                                JOptionPane.showMessageDialog(null, "Erro: Combinação Data/Hora/Doutor(a) indisponível");
                                break;
                            } else {
                                salasOcupadas.add(c.getSala());
                            }
                        } else if (dataHoraInicio.isAfter(c.getDatainicio()) && dataHoraFim.isBefore(c.getDatafim())) {
                            if (c.getIddent().getNome().equals(dentistaSelect.getSelectedItem())) {
                                hourOccupied = true;
                                JOptionPane.showMessageDialog(null, "Erro: Combinação Data/Hora/Doutor(a) indisponível");
                                break;
                            } else {
                                salasOcupadas.add(c.getSala());
                            }
                        } else if (dataHoraInicio.equals(c.getDatainicio())) {
                            if (c.getIddent().getNome().equals(dentistaSelect.getSelectedItem())) {
                                hourOccupied = true;
                                JOptionPane.showMessageDialog(null, "Erro: Combinação Data/Hora/Doutor(a) indisponível");
                                break;
                            } else {
                                salasOcupadas.add(c.getSala());
                            }
                        }
                    }


                    if (!hourOccupied) {
                        Random rnd = new Random();
                        int sala = getRandomRoom(rnd, 1, 12, salasOcupadas);
                        Consulta.addConsulta(datePickerTxt.getValue().toString(), horaTxt.getSelectedItem().toString(), sala,
                                dentistas.get(dentistaSelect.getSelectedIndex()).getIddent(), utentes.get(utenteSelect.getSelectedIndex()).getIdutente(),
                                proceds.get(procedSelect.getSelectedIndex()).getIdproced());

                        JComponent comp = (JComponent) e.getSource();
                        Window win = SwingUtilities.getWindowAncestor(comp);
                        win.dispose();
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Preencha todos os campos.");
                }
            }
        });
        datePickerTxt.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                cal.setLocation(datePickerTxt.getLocationOnScreen().x,
                        (datePickerTxt.getLocationOnScreen().y + datePickerTxt.getHeight()));

                //String selectedDate = (String)datePickerTxt.getValue();
                //cal.resetSelection(java.sql.Timestamp.valueOf(selectedDate));

                cal.setVisible(true);
            }
        });
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        if (evt.getPropertyName().equals("selectedDate")) {
            Calendar cal = (Calendar) evt.getNewValue();
            LocalDateTime selectedDate = cal.getTime().toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDateTime();
            datePickerTxt.setValue(formatterData.format(selectedDate));
        }
    }

    public void populateComboBoxes(List<Utente> utentes, List<Dentista> dentistas, List<Procedimento> proceds) {

        for (Utente u : utentes) {
            utenteSelect.addItem(u.getNome());
        }

        for (Dentista d : dentistas) {
            dentistaSelect.addItem(d.getNome());
        }

        for (Procedimento p : proceds) {
            procedSelect.addItem(p.getNome());
        }

        for (int i = 8; i <= 19; i++) {
            if (i < 10) {
                horaTxt.addItem("0" + i + ":00");
                horaTxt.addItem("0" + i + ":30");
            } else {
                horaTxt.addItem(i + ":00");
                horaTxt.addItem(i + ":30");
            }

            AutoCompleteDecorator.decorate(utenteSelect);
            AutoCompleteDecorator.decorate(dentistaSelect);
            AutoCompleteDecorator.decorate(procedSelect);
        }
    }

    public int getRandomRoom(Random rnd, int start, int end, List<Integer> exclude) {
        int random = start + rnd.nextInt(end - start + 1 - exclude.size());
        for (int ex : exclude) {
            if (random < ex) {
                break;
            }
            random++;
        }
        return random;
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        MarcarConsultaPnl = new JPanel();
        MarcarConsultaPnl.setLayout(new GridLayoutManager(7, 2, new Insets(0, 0, 0, 0), -1, -1));
        final JLabel label1 = new JLabel();
        label1.setText("Data:");
        MarcarConsultaPnl.add(label1, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("Hora:");
        MarcarConsultaPnl.add(label2, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        utenteSelect = new JComboBox();
        MarcarConsultaPnl.add(utenteSelect, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label3 = new JLabel();
        label3.setText("Utente:");
        MarcarConsultaPnl.add(label3, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label4 = new JLabel();
        label4.setText("Procedimento:");
        MarcarConsultaPnl.add(label4, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        procedSelect = new JComboBox();
        MarcarConsultaPnl.add(procedSelect, new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label5 = new JLabel();
        label5.setText("Doutor(a):");
        MarcarConsultaPnl.add(label5, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        dentistaSelect = new JComboBox();
        MarcarConsultaPnl.add(dentistaSelect, new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        marcarConsultaBtn = new JButton();
        marcarConsultaBtn.setText("Marcar Consulta");
        MarcarConsultaPnl.add(marcarConsultaBtn, new GridConstraints(6, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        datePickerTxt = new JFormattedTextField();
        MarcarConsultaPnl.add(datePickerTxt, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        horaTxt = new JComboBox();
        MarcarConsultaPnl.add(horaTxt, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return MarcarConsultaPnl;
    }

}