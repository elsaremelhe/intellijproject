package UIForms;

import javax.swing.*;

public class MainGUI extends JFrame {

    public MainGUI(){
        LogIn loginform = new LogIn();
        this.getContentPane().add(loginform.getLogInPanel());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args){
        MainGUI frame = new MainGUI();

        frame.setTitle("Log In");
        frame.pack();
        frame.setVisible(true);
        frame.setSize(500,250);
        frame.setLocationRelativeTo(null); //para a janela aparecer no centro
    }
}
